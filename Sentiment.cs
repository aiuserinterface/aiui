﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace AIdemo
{
    public class Sentiment
    {
        string _uuid = string.Empty;
        float _negative = 0;
        float _neutral = 0;
        float _positive = 0;
        float _compound = 0;
        string _negativeWords = string.Empty;
        string _positiveWords = string.Empty;
        string _error = string.Empty;

        public string TransactionId { get { return _uuid; } set { _uuid = value; } }
        public float Negative { get { return _negative; } set { _negative = value; } }
        public float Neutral { get { return _neutral; } set { _neutral = value; } }
        public float Positive { get { return _positive; } set { _positive = value; } }
        public float Compound { get { return _compound; } set { _compound = value; } }
        public string NegativeWords { get { return _negativeWords; } set { _negativeWords = value; } }
        public string PositiveWords { get { return _positiveWords; } set { _positiveWords = value; } }
        public string Error { get { return _error; } }

        private Sentiment() { }

        public Sentiment(string AId)
        {
            _uuid = AId;
        }

        public bool Load(MySqlConnection AConnection)
        {
            bool result = false;
            try
            {
                if (AConnection != null)
                {
                    if (AConnection.State != System.Data.ConnectionState.Open)
                        AConnection.Open();
                    if (AConnection.State == System.Data.ConnectionState.Open)
                    {
                        MySqlCommand comm = new MySqlCommand("SELECT *, LENGTH(Neg_words) AS Neg_words_length, LENGTH(Pos_words) AS Pos_words_length FROM sentiment WHERE UUID = @Id", AConnection);
                        comm.Parameters.AddWithValue("Id", _uuid);
                        MySqlDataReader reader = comm.ExecuteReader();
                        if (reader.HasRows)
                        {
                            if (reader.Read())
                            {
                                result = this.Read(reader);
                            }
                        }
                        comm.Dispose();
                        reader.Close();
                        reader.Dispose();
                    }
                }
            }
            catch (Exception exc) { _error = exc.Message; }
            return result;
        }

        public bool Read(MySqlDataReader AReader)
        {
            bool result = false;
            if (AReader != null)
            {
                try
                {
                    _uuid = AReader.GetString(AReader.GetOrdinal("UUID"));
                    _negative = AReader.GetFloat(AReader.GetOrdinal("Negative_val"));
                    _neutral = AReader.GetFloat(AReader.GetOrdinal("Neutral_val"));
                    _positive = AReader.GetFloat(AReader.GetOrdinal("Positive_val"));
                    _compound = AReader.GetFloat(AReader.GetOrdinal("Compount_val"));

                    byte[] buf = new byte[AReader.GetInt32(AReader.GetOrdinal("Neg_words_length"))];
                    AReader.GetBytes(AReader.GetOrdinal("Neg_words"), 0, buf, 0, AReader.GetInt32(AReader.GetOrdinal("Neg_words_length")));
                    _negativeWords = System.Text.Encoding.UTF8.GetString(buf, 0, AReader.GetInt32(AReader.GetOrdinal("Neg_words_length")));

                    buf = new byte[AReader.GetInt32(AReader.GetOrdinal("Pos_words_length"))];
                    AReader.GetBytes(AReader.GetOrdinal("Pos_words"), 0, buf, 0, AReader.GetInt32(AReader.GetOrdinal("Pos_words_length")));
                    _positiveWords = System.Text.Encoding.UTF8.GetString(buf, 0, AReader.GetInt32(AReader.GetOrdinal("Pos_words_length")));

                    result = true;
                }
                catch (Exception exc) { _error = exc.Message; }
            }
            return result;

        }
    }

}
