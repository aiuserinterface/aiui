﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIdemo
{
    public class BroadcastMessage
    {
        //  UDP message format
        //  Transaction ID = GUID | Timestamp = yyyy/MM/dd HH:mm:ss:zzz | Process type = text | Error code = text | Message = text

        Guid _transactionId = new Guid();
        DateTime _timestamp;
        string _processType = string.Empty;
        string _errorCode = string.Empty;
        string _message = string.Empty;
        string _readError = string.Empty;

        public Guid TransactionId { get { return _transactionId; } set { _transactionId = value; } }
        public DateTime TimeStamp { get { return _timestamp; } set { _timestamp = value; } }
        public string ProcessType { get { return _processType; } set { _processType = value; } }
        public string ErrorCode { get { return _errorCode; } set { _errorCode = value; } }
        public string Message { get { return _message; } set { _message = value; } }
        public string ReadError { get { return _readError; } }

        public bool Read(string ABroadcastMessage)
        {
            bool result = false;
            try
            {
                string[] split = ABroadcastMessage.Split('|');
                if (split.Length == 5)
                {
                    _transactionId = new Guid(split[0].Trim());

                    //01234567890123456789012
                    //yyyy/MM/dd HH:mm:ss:zzz

                    int y = Convert.ToInt32(split[1].Trim().Substring(0, 4));
                    int M = Convert.ToInt32(split[1].Trim().Substring(5, 2));
                    int d = Convert.ToInt32(split[1].Trim().Substring(8, 2));
                    int h = Convert.ToInt32(split[1].Trim().Substring(11, 2));
                    int m = Convert.ToInt32(split[1].Trim().Substring(14, 2));
                    int s = Convert.ToInt32(split[1].Trim().Substring(17, 2));                    
                    _timestamp = new DateTime(y, M, d, h, m, s);

                    _processType = split[2];
                    _errorCode = split[3];
                    _message = split[4];

                    result = true;
                }
            }
            catch (Exception exc)
            {
                _readError = exc.Message;
            }
            return result;
        }
    }
}
