﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace AIdemo
{
    public class VoiceLog
    {
        string _uuid = string.Empty;
        string _encoding = string.Empty;
        int _channels = 0;
        int _sampleSize = 0;
        int _sampleRate = 0;
        TimeSpan _fileDuration = TimeSpan.FromSeconds(0);
        string _type = string.Empty;

        Compliance _compliance = null;
        Sentiment _sentiment = null;
        string _error = string.Empty;

        public string UUID { get { return _uuid; } set { _uuid = value; } }
        public string Encoding { get { return _encoding; } set { _encoding = value; } }
        public int Channels { get { return _channels; } set { _channels = value; } }
        public int SampleSize { get { return _sampleSize; } set { _sampleSize = value; } }
        public int SampleRate { get { return _sampleRate; } set { _sampleRate = value; } }        
        public TimeSpan FileDuration { get { return _fileDuration; } set { _fileDuration = value; } }
        public string Type { get { return _type; } set { _type = value; } }

        public Compliance Compliance { get { return _compliance; } set { _compliance = value; } }
        public Sentiment Sentiment { get { return _sentiment; } set { _sentiment = value; } }
        public string Error { get { return _error; } }

        public bool Load(MySqlConnection AConnection, bool ALoadCompliance, bool ALoadSentiment)
        {
            bool result = false;
            try
            {
                if (AConnection != null)
                {
                    if (AConnection.State != System.Data.ConnectionState.Open)
                        AConnection.Open();
                    if (AConnection.State == System.Data.ConnectionState.Open)
                    {
                        MySqlCommand comm = new MySqlCommand("SELECT * FROM file_meta_data WHERE UUID = @UUID", AConnection);
                        try
                        {
                            comm.Parameters.AddWithValue("UUID", _uuid);
                            MySqlDataReader reader = comm.ExecuteReader();
                            if (reader.HasRows)
                            {
                                if (reader.Read())
                                {
                                    this.Read(reader);
                                    if (ALoadCompliance) { _compliance = new Compliance(_uuid); }
                                    if (ALoadSentiment) { _sentiment = new Sentiment(_uuid); }
                                    result = true;
                                }
                            }
                            reader.Close();
                            reader.Dispose();
                        }
                        catch { }
                        finally
                        {                           
                            comm.Dispose();
                        }
                    }
                }
            }
            catch { }
            return result;
        }

        public bool Read(MySqlDataReader AReader)
        {
            bool result = false;
            try
            {
                if (AReader != null)
                {
                    _uuid = AReader.GetString(AReader.GetOrdinal("UUID"));
                    _encoding = AReader.GetString(AReader.GetOrdinal("Encoding"));
                    _channels = AReader.GetInt32(AReader.GetOrdinal("Channels"));
                    _sampleSize = AReader.GetInt32(AReader.GetOrdinal("SampleSize"));
                    _sampleRate = AReader.GetInt32(AReader.GetOrdinal("SampleRate"));
                    //_fileDuration = DateTime.FromOADate(AReader.GetDouble(AReader.GetOrdinal("FileDuration")));
                    _fileDuration = TimeSpan.FromSeconds(AReader.GetDouble(AReader.GetOrdinal("FileDuration")));
                    _type = AReader.GetString(AReader.GetOrdinal("Type"));
                    result = true;
                }                
            }
            catch (Exception exc)
            {
                _error = exc.Message;
            }
            return result;
        }
    }

    public enum MetaFileType { All, Mail, Voice };

    public class VoiceLogs : List<VoiceLog>
    {
        string _error = string.Empty;
        public string Error { get { return _error; } }

        public bool Load(MySqlConnection AConnection, string AConnectionString, MetaFileType AFileType, String AId)
        {
            this.Clear();

            bool result = false;
            try
            {
                if (AConnection != null)
                {
                    if (AConnection.State != System.Data.ConnectionState.Open)
                        AConnection.Open();
                    if (AConnection.State == System.Data.ConnectionState.Open)
                    {
                        MySqlConnection connCompliance = new MySqlConnection(AConnectionString);
                        connCompliance.Open();
                        MySqlConnection connSentiment = new MySqlConnection(AConnectionString);
                        connSentiment.Open();
                        try
                        {
                            MySqlCommand comm = new MySqlCommand();
                            comm.Connection = AConnection;
                            if (AId == string.Empty)
                            {
                                switch (AFileType)
                                {
                                    case MetaFileType.All:
                                        comm.CommandText = "SELECT * FROM file_meta_data ORDER BY FileDuration DESC";
                                        break;
                                    case MetaFileType.Mail:
                                        comm.CommandText = "SELECT * FROM file_meta_data WHERE Type = 'EML' ORDER BY FileDuration DESC";
                                        break;
                                    case MetaFileType.Voice:
                                        comm.CommandText = "SELECT * FROM file_meta_data WHERE Type = 'VOX' ORDER BY FileDuration DESC";
                                        break;
                                }
                            }
                            else
                            {
                                switch (AFileType)
                                {
                                    case MetaFileType.All:
                                        comm.CommandText = "SELECT * FROM file_meta_data WHERE UUID = @Id ORDER BY FileDuration DESC";
                                        comm.Parameters.AddWithValue("Id", AId);
                                        break;
                                }
                            }
                            
                            MySqlDataReader reader = comm.ExecuteReader();
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    VoiceLog voicelog = new VoiceLog();
                                    if (voicelog.Read(reader))
                                    {
                                        voicelog.Compliance = new Compliance(voicelog.UUID);
                                        voicelog.Compliance.Load(connCompliance);
                                        voicelog.Sentiment = new Sentiment(voicelog.UUID);
                                        voicelog.Sentiment.Load(connSentiment);
                                        this.Add(voicelog);
                                    }
                                }
                            }
                            reader.Close();
                            reader.Dispose();
                            comm.Dispose();
                        }
                        catch { }
                        finally
                        {
                            if (connSentiment.State == System.Data.ConnectionState.Open) connSentiment.Close();
                            if (connCompliance.State == System.Data.ConnectionState.Open) connCompliance.Close();
                            connSentiment.Dispose();
                            connCompliance.Dispose();
                        }

                    }
                    result = true;
                }
            }
            catch (Exception exc) { _error = exc.Message; }
            return result;
        }
    }


}
