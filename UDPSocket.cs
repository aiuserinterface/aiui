﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace AIdemo
{
    public delegate void ListeningHandler(object ASender);
    public delegate void ClosedHandler(object ASender);
    public delegate void MessageReceivedHandler(object ASender, IPAddress AIP, string AMessage, int ALen);
    public delegate void ListenerErrorHandler(object ASender, string AMessage);

    public class UDPSocket
    {
        public event ListeningHandler OnListening;
        public event ClosedHandler OnClosed;
        public event MessageReceivedHandler OnMessageReceived;
        public event ListenerErrorHandler OnError;

        public UdpState State { get { return state; } }
        public Int32 Port { get { return port; } set { port = value; } }

        public enum UdpState { Closed, Listening };

        UdpClient client = null;
        UdpState state = UdpState.Closed;
        Int32 port;

        public bool Listen()
        {
            bool result = false;
            try
            {
                IPAddress _ipAddress;
                IPAddress.TryParse("0.0.0.0", out _ipAddress);
                IPEndPoint _ipEndPoint = new IPEndPoint(_ipAddress, port);
                if (client == null)
                    client = new UdpClient(_ipEndPoint);
                else
                {
                    client.Close();
                    client = new UdpClient(_ipEndPoint);
                }
                if (client != null)
                {
                    client.BeginReceive(new AsyncCallback(ReceiveMessage), client);
                    state = UdpState.Listening;
                    if (OnListening != null)
                    { OnListening(this); }
                }
            }
            catch (Exception exc)
            {
                if (OnError != null)
                { OnError(this, "Error opening listener: " + exc.Message + "\n"); }
            }
            return result;
        }

        public bool Close()
        {
            bool result = false;
            try
            {
                client.Close();
                state = UdpState.Closed;
                result = true;
                if (OnClosed != null)
                { OnClosed(this); }
            }
            catch (Exception exc)
            {
                if (OnError != null)
                { OnError(this, "Error closing listener: " + exc.Message + "\n"); }
            }
            return result;
    }

        private void ReceiveMessage(IAsyncResult iar)
        {
            if ((state == UdpState.Listening) && (client != null) && (client.Client != null))
            {
                if ((client != null) && (client.Client != null))
                {
                    try
                    {
                        IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, port);
                        byte[] received = client.EndReceive(iar, ref RemoteIpEndPoint);

                        try
                        {
                            int len = received.Length;
                            MessageReceivedHandler m = new MessageReceivedHandler(MessageReceived);
                            m.Invoke(null, RemoteIpEndPoint.Address, Encoding.UTF8.GetString(received), received.Length);
                        }
                        finally
                        {
                            if ((client != null) && (client.Client != null) && (state == UdpState.Listening))
                            { client.BeginReceive(new AsyncCallback(ReceiveMessage), null); }
                        }
                    }
                    catch (Exception exc)
                    {
                        if ((client != null) && (client.Client != null))
                        {
                            if (OnError != null)
                            { OnError(this, "Error receiving: " + exc.Message + "\n"); }
                        }
                    }
                }
            }
        }

        private void MessageReceived(object ASender, IPAddress AIP, string AMessage, int ALen)
        {
            if (OnMessageReceived != null)
            {
                OnMessageReceived(ASender, AIP, AMessage, ALen);
            }
        }
    }
}
