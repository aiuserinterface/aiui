﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Windows;
using System.Threading;
using Gma.CodeCloud.Base;
using Gma.CodeCloud.Base.FileIO;
using Gma.CodeCloud.Base.Geometry;
using Gma.CodeCloud.Base.TextAnalyses.Blacklist;
using Gma.CodeCloud.Base.TextAnalyses.Processing;
using Gma.CodeCloud.Base.TextAnalyses.Stemmers;
using Gma.CodeCloud.Controls;
using MySql;
using MySql.Data;
using MySql.Data.MySqlClient;
using NAudio;
using NAudio.Wave;


namespace AIdemo
{
    public partial class formMain : Form
    {
        public static MetaFileType fMeta = MetaFileType.All;
        public static VoiceLogs fVoiceLogs = null;
        public static VoiceLog fVoiceLog = null;

        private CancellationTokenSource fCancelSourceIndividual;
        private CancellationTokenSource fCancelSource;
        public static int fTotalWordCountIndividual = 0;
        public static int fTotalWordCount = 0;

        public NAudio.Wave.WaveIn fSourceStream = null;
        public NAudio.Wave.DirectSoundOut fWaveOut = null;
        public NAudio.Wave.WaveFileWriter fWaveWriter = null;

        public static TCPSocket fSocket = null;

        public formMain()
        {
            InitializeComponent();
        }

        public static UDPSocket udp = null;

        private void Form1_Load(object sender, EventArgs e)
        {
            textFtpServer.Text = Properties.Settings.Default.ftpserver;
            textFtpUser.Text = Properties.Settings.Default.ftpuser;
            textFtpPass.Text = Properties.Settings.Default.ftppassword;
            textFtpDir.Text = Properties.Settings.Default.ftpremotedir;

            textTCPServer.Text = Properties.Settings.Default.tcpserver;
            textTCPPort.Text = Properties.Settings.Default.tcpport;

            teConnectionString.Text = Properties.Settings.Default.db.ToString();

            buUDPListen_Click(sender, e);
            GetAudioDevices();

            fMeta = MetaFileType.All;
            //LoadLogs(fMeta);
        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            
        }

        private void buUDPListen_Click(object sender, EventArgs e)
        {
            if (udp == null)
            {
                udp = new UDPSocket();
                udp.OnClosed += udp_OnClosed;
                udp.OnError += udp_OnError;
                udp.OnListening += udp_OnListening;
                udp.OnMessageReceived += udp_OnMessageReceived;
            }
            if (udp.State == UDPSocket.UdpState.Closed)
            {
                udp.Port = Convert.ToInt32(tePort.Text.Trim());                
                udp.Listen();
            }
            else
            {
                teUDPLog.AppendText("UDP client already listening on " + udp.Port.ToString() + "\n");
            }
        }

        delegate void MessageReceivedCallback(object ASender, IPAddress AIP, string AMessage, int ALen);
        public void udp_OnMessageReceived(object ASender, IPAddress AIP, string AMessage, int ALen)
        {
            if (this.teUDPLog.InvokeRequired)
            {
                MessageReceivedCallback m = new MessageReceivedCallback(udp_OnMessageReceived);
                this.Invoke(m, new object[] { ASender, AIP, AMessage, ALen });
            }
            else
            {                
                teUDPLog.AppendText("Message received: (" + AIP.ToString() + ") [" + ALen.ToString() + " bytes]\n");
                teUDPLog.AppendText(System.Environment.NewLine);
                //teUDPLog.AppendText(AMessage);
                teUDPLog.AppendText(System.Environment.NewLine);
                BroadcastMessage bcm = new BroadcastMessage();
                if (bcm.Read(AMessage))
                {
                    teUDPLog.AppendText(" - TxID: " + bcm.TransactionId.ToString() + "\n");
                    teUDPLog.AppendText(" - Timestamp: " + bcm.TimeStamp.ToString() + "\n");
                    teUDPLog.AppendText(" - Process type: " + bcm.ProcessType + "\n");
                    teUDPLog.AppendText(" - Error code: " + bcm.ErrorCode + "\n");
                    teUDPLog.AppendText(" - Message: " + bcm.Message + "\n");
                    teUDPLog.AppendText(System.Environment.NewLine);

                    toolStripButton1_Click(this, null);

                    if (bcm.TransactionId.ToString() == textSampleId.Text)
                    {
                        if (bcm.ProcessType == "V2T") listViewEvents.Items[0].ImageIndex = 1;
                        if (bcm.ProcessType == "SENT") listViewEvents.Items[1].ImageIndex = 1;
                        if (bcm.ProcessType == "COMP") listViewEvents.Items[2].ImageIndex = 1;
                    }
                }
                else{
                    teUDPLog.AppendText(" - Error reading BCM: " + bcm.ReadError + "\n");
                }
            }
        }

        delegate void ErrorCallback(object ASender, string AMessage);
        void udp_OnError(object ASender, string AMessage)
        {
            if (this.teUDPLog.InvokeRequired)
            {
                ErrorCallback e = new ErrorCallback(udp_OnError);
                this.Invoke(e, new object[] { ASender, AMessage });
            }
            else
                teUDPLog.AppendText("UDP error - " + AMessage + "\n");
        }
        
        void udp_OnListening(object ASender)
        {
            teUDPLog.AppendText("UDP client listening...\n");
        }        

        void udp_OnClosed(object ASender)
        {

            teUDPLog.AppendText("UDP client closed\n");
        }

        public void ReceiveCallback(IAsyncResult ar)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if ((udp != null) && (udp.State == UDPSocket.UdpState.Listening))
            {
                udp.Close();
            }
        }

        private void buUDPSend_Click(object sender, EventArgs e)
        {
            //  UDP message format
            //  Transaction ID = GUID | Timestamp = yyyy/MM/dd HH:mm:ss:zzz | Process type = text | Error code = text | Message = text

            UdpClient client = new UdpClient();
            IPEndPoint ip = new IPEndPoint(IPAddress.Broadcast, Convert.ToInt32(tePort.Text.Trim()));

            string message = Guid.NewGuid().ToString() + "|" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.fff") + "|TEST|Err0|This is a test";
            byte[] bytes = Encoding.ASCII.GetBytes(message);
            
            client.Send(bytes, bytes.Length, ip);
            client.Close();

        }

        private void panelWordCloud_Click(object sender, EventArgs e)
        {

            
        }

        private void label3_Click(object sender, EventArgs e)
        {
            
        }

        private void buClearLog_Click(object sender, EventArgs e)
        {
            teUDPLog.Text = string.Empty;
        }

        private void buConnectDB_Click(object sender, EventArgs e)
        {
            MySqlConnection conn = new MySqlConnection();
            try
            {
                conn.ConnectionString = teConnectionString.Text.Trim();
                conn.Open();
                if (conn.State == ConnectionState.Open)
                {
                    teDatabaseLog.AppendText("Connected to database!\n");
                    conn.Close();
                }
            }
            catch (Exception exc)
            {
                teDatabaseLog.AppendText("Error connecting to database: " + exc.Message + "\n");
            }
            finally
            {
                conn.Dispose();
            }
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
                if (tabPages.TabPages.IndexOf(tabNetworking) != -1)
                    tabPages.TabPages.Remove(tabNetworking);
                if (tabPages.TabPages.IndexOf(tabVoiceRecorder) != -1)
                    tabPages.TabPages.Remove(tabVoiceRecorder);
                if (tabPages.TabPages.IndexOf(tabDatabase) != -1)
                    tabPages.TabPages.Remove(tabDatabase);
            }
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Maximized)
            {
                this.WindowState = FormWindowState.Normal;
                if (tabPages.TabPages.IndexOf(tabNetworking) == -1)
                    tabPages.TabPages.Add(tabNetworking);
                if (tabPages.TabPages.IndexOf(tabVoiceRecorder) == -1)
                    tabPages.TabPages.Add(tabVoiceRecorder);
                if (tabPages.TabPages.IndexOf(tabDatabase) == -1)
                    tabPages.TabPages.Add(tabDatabase);
            }
        }

        private void formMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.db = teConnectionString.Text.Trim();

            Properties.Settings.Default.tcpserver = textTCPServer.Text.Trim();
            Properties.Settings.Default.tcpport = textTCPPort.Text.Trim();

            Properties.Settings.Default.Save();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            
        }

        public void BuildSummary()
        {            
            listVoiceLogs.BeginUpdate();
            try
            {
                listVoiceLogs.Items.Clear();
                foreach (VoiceLog vl in fVoiceLogs)
                {

                    //  VOICE LOG 
                    ListViewItem item = new ListViewItem();
                    item.Text = " " + vl.UUID;
                    if (vl.Type == "VOX")
                        item.ImageIndex = 0;
                    else
                        item.ImageIndex = 1;                   

                    item.SubItems.Add(new ListViewItem.ListViewSubItem(item, vl.Encoding));
                    item.SubItems.Add(new ListViewItem.ListViewSubItem(item, vl.Channels.ToString()));
                    item.SubItems.Add(new ListViewItem.ListViewSubItem(item, vl.SampleSize.ToString()));
                                       
                    
                    if (vl.SampleRate >= 1000)
                    {
                        double sampleRate = vl.SampleRate / 1000;
                        item.SubItems.Add(new ListViewItem.ListViewSubItem(item, sampleRate.ToString() + "kHz"));
                    }
                    else
                        item.SubItems.Add(new ListViewItem.ListViewSubItem(item, vl.SampleRate.ToString() + "Hz"));

                    item.SubItems.Add(new ListViewItem.ListViewSubItem(item, vl.FileDuration.ToString(@"hh\:mm\:ss\.fff")));
                    listVoiceLogs.Items.Add(item);

                    //  COMPLIANCE FLAGS
                    //if (vl.Compliance != null)
                    //{
                    //    item.SubItems.Add(new ListViewItem.ListViewSubItem(item, vl.Compliance.Greeting.ToString()));
                    //    item.SubItems.Add(new ListViewItem.ListViewSubItem(item, vl.Compliance.DisclaimerRead.ToString()));
                    //    item.SubItems.Add(new ListViewItem.ListViewSubItem(item, vl.Compliance.DisclaimerAccepted.ToString()));
                    //    item.SubItems.Add(new ListViewItem.ListViewSubItem(item, vl.Compliance.Profanity.ToString()));
                    //}
                    //else
                    //{
                    //    for (int i = 0; i < 4; i++)
                    //        item.SubItems.Add(new ListViewItem.ListViewSubItem(item, ""));
                    //}

                    for (int i = 0; i < listVoiceLogs.Columns.Count; i++)
                    {
                        listVoiceLogs.Columns[i].AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent);
                        listVoiceLogs.Columns[i].Width = -2;
                    }
                }
            }
            finally
            {
                listVoiceLogs.EndUpdate();
            }
            BuildCloud();
            ChartOverallSentiment();
        }

        public void BuildCloud()
        {
            Language language = ByLanguageFactory.GetLanguageFromString("English *.txt");
            FileIterator fileIterator = ByLanguageFactory.GetFileIterator(language);

            IBlacklist blacklist = ByLanguageFactory.GetBlacklist(language);
            IWordStemmer stemmer = ByLanguageFactory.GetStemmer(language);

            cloudControl.WeightedWords = new List<IWord>();
            fCancelSource = new CancellationTokenSource();
            Task.Factory
                .StartNew(
                    () => GetWordsParallely(language, blacklist, stemmer), fCancelSource.Token)
                .ContinueWith(
                    ApplyResults);
        }

        private void ApplyResults(Task<List<IWord>> task)
        {
            //cloudControl.WeightedWords = new List<IWord>();
            //cloudControl.WeightedWords = task.Result;            
            //fTotalWordCount = cloudControl.WeightedWords.Sum(word => word.Occurrences);

            this.Invoke(
                new Action(
                    () =>
                    {
                        if (task.IsCanceled)
                        {
                            cloudControl.WeightedWords = new List<IWord>();
                        }
                        else
                        {
                            if (task.IsFaulted && task.Exception != null)
                            {
                                throw task.Exception;
                            }
                            cloudControl.WeightedWords = task.Result;
                        }

                        fTotalWordCount = cloudControl.WeightedWords.Sum(word => word.Occurrences);
                    }));
        }

        private List<IWord> GetWordsParallely(Language language, IBlacklist blacklist, IWordStemmer stemmer)
        {
            string sentiment = string.Empty;
            foreach (VoiceLog vl in fVoiceLogs)
            {
                sentiment += vl.Sentiment.NegativeWords + vl.Sentiment.PositiveWords;// +vl.Sentiment.NegativeWords;
            }

            string[] keywords = ExtractKeywords(sentiment);            

            return keywords
                .AsParallel()
                .WithCancellation(fCancelSource.Token)                                
                .Filter(blacklist)
                .CountOccurences()
                .GroupByStem(stemmer)
                .SortByOccurences()
                .AsEnumerable()
                .Cast<IWord>()
                .ToList();
        }

        public string[] ExtractKeywords(string AInput)
        {
            string[] result = { };
            string[] arr = Regex.Split(AInput, @"(\[\(')|(',\s)|(\),\s\(')|(\))|(\)\])|(])", RegexOptions.ExplicitCapture);
            
            List<string> listWord = new List<string>();
            List<int> listCount = new List<int>();

            foreach (string str in arr)
            {
                if ((str != "") && (str != "[") && (str != "]"))
                {
                    int count = 0;
                    if (int.TryParse(str, out count))
                    {
                        listCount.Add(count);
                    }
                    else
                    {
                        listWord.Add(str);
                    }
                }
            }

            if (listWord.Count == listCount.Count)
            {
                List<string> listResult = new List<string>();
                foreach (string keyword in listWord)
                {
                    int index = listWord.IndexOf(keyword);
                    for (int j = 1; j <= listCount[index]; j++)
                        listResult.Add(keyword);
                }
                listResult.Sort();
                result = listResult.ToArray();    
            }
            return result;
        }

        private void coChart_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChartOverallSentiment();
        }

        private void ChartOverallSentiment()
        {
            try
            {
                if (fVoiceLogs.Count > 0)
                {
                    if (coChart.SelectedIndex == -1) coChart.SelectedIndex = 0;

                    chartSentiment.Series.Clear();

                    float negative = 0;
                    float neutral = 0;
                    float positive = 0;

                    foreach (VoiceLog vl in fVoiceLogs)
                    {
                        negative += vl.Sentiment.Negative;
                        neutral += vl.Sentiment.Neutral;
                        positive += vl.Sentiment.Positive;
                    }

                    negative = negative / fVoiceLogs.Count;
                    neutral = neutral / fVoiceLogs.Count;
                    positive = positive / fVoiceLogs.Count;

                    if (coChart.SelectedIndex == 0)
                    {
                        System.Windows.Forms.DataVisualization.Charting.Series series = chartSentiment.Series.Add("Sentiment");
                        chartSentiment.Series["Sentiment"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
                        chartSentiment.Series["Sentiment"].SmartLabelStyle.AllowOutsidePlotArea = System.Windows.Forms.DataVisualization.Charting.LabelOutsidePlotAreaStyle.Yes;

                        System.Windows.Forms.DataVisualization.Charting.DataPoint dp;
                        dp = chartSentiment.Series["Sentiment"].Points.Add(negative);
                        dp.Label = "Negative: " + negative.ToString();
                        dp.LabelForeColor = Color.White;

                        dp = chartSentiment.Series["Sentiment"].Points.Add(neutral);
                        dp.Label = "Neutral: " + neutral.ToString();
                        dp.LabelForeColor = Color.White;

                        dp = chartSentiment.Series["Sentiment"].Points.Add(positive);
                        dp.Label = "Positive: " + positive.ToString();
                        dp.LabelForeColor = Color.White;

                        chartSentiment.Series["Sentiment"].Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.BrightPastel;

                        chartSentiment.Series["Sentiment"].ChartArea = "ChartArea1";
                        chartSentiment.Series["Sentiment"].Font = new System.Drawing.Font(this.Font.FontFamily, this.Font.Size, FontStyle.Regular);
                    }

                    if (coChart.SelectedIndex == 1)
                    {
                        System.Windows.Forms.DataVisualization.Charting.Series series = chartSentiment.Series.Add("Sentiment");
                        chartSentiment.Series["Sentiment"].Color = Color.SteelBlue;
                        chartSentiment.Series["Sentiment"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;

                        System.Windows.Forms.DataVisualization.Charting.DataPoint dp;
                        dp = chartSentiment.Series["Sentiment"].Points.Add(negative);
                        dp.Label = "Negative";

                        dp = chartSentiment.Series["Sentiment"].Points.Add(neutral);
                        dp.Label = "Neutral";

                        dp = chartSentiment.Series["Sentiment"].Points.Add(positive);
                        dp.Label = "Positive";

                        chartSentiment.Series["Sentiment"].ChartArea = "ChartArea1";
                        chartSentiment.Series["Sentiment"].Font = new System.Drawing.Font(this.Font.FontFamily, this.Font.Size, FontStyle.Regular);

                        chartSentiment.ChartAreas["ChartArea1"].AxisX.LabelStyle.Font = new System.Drawing.Font(this.Font.FontFamily, this.Font.Size, FontStyle.Regular);
                        chartSentiment.ChartAreas["ChartArea1"].AxisY.LabelStyle.Font = new System.Drawing.Font(this.Font.FontFamily, this.Font.Size, FontStyle.Regular);

                        chartSentiment.ChartAreas["ChartArea1"].AxisY.Title = "Sentiment";

                        
                    }
                }
            }
            catch { }
        }

        private void LoadIndividual()
        {
            if (fVoiceLog != null)
            {
                tableIndividual.Controls.Clear();

                Label label;

                label = new Label();                
                label.AutoSize = true;
                label.Parent = this;
                label.Text = "UUID";
                label.Font = new System.Drawing.Font(this.Font.FontFamily, this.Font.Size, FontStyle.Bold);
                tableIndividual.Controls.Add(label,0,0);

                label = new Label();
                label.AutoSize = true;
                label.Parent = this;
                label.Text = fVoiceLog.UUID;
                tableIndividual.Controls.Add(label,0,1);                

                label = new Label();
                label.AutoSize = true;
                label.Parent = this;
                label.Text = "Encoding";
                label.Font = new System.Drawing.Font(this.Font.FontFamily, this.Font.Size, FontStyle.Bold);
                tableIndividual.Controls.Add(label, 1, 0);

                label = new Label();
                label.AutoSize = true;
                label.Parent = this;
                label.Text = fVoiceLog.Encoding;
                tableIndividual.Controls.Add(label, 1, 1);

                label = new Label();
                label.AutoSize = true;
                label.Parent = this;
                label.Text = "Channels";
                label.Font = new System.Drawing.Font(this.Font.FontFamily, this.Font.Size, FontStyle.Bold);
                tableIndividual.Controls.Add(label, 2, 0);

                label = new Label();
                label.AutoSize = true;
                label.Parent = this;
                label.Text = fVoiceLog.Channels.ToString();
                tableIndividual.Controls.Add(label, 2, 1);

                label = new Label();
                label.AutoSize = true;
                label.Parent = this;
                label.Text = "Sample size";
                label.Font = new System.Drawing.Font(this.Font.FontFamily, this.Font.Size, FontStyle.Bold);
                tableIndividual.Controls.Add(label, 0, 2);

                label = new Label();
                label.AutoSize = true;
                label.Parent = this;
                label.Text = fVoiceLog.SampleSize.ToString();
                tableIndividual.Controls.Add(label, 0, 3);

                label = new Label();
                label.AutoSize = true;
                label.Parent = this;
                label.Text = "Sample rate";
                label.Font = new System.Drawing.Font(this.Font.FontFamily, this.Font.Size, FontStyle.Bold);
                tableIndividual.Controls.Add(label, 1, 2);

                /*if (vl.SampleRate >= 1000)
                    {
                        double sampleRate = vl.SampleRate / 1000;
                        item.SubItems.Add(new ListViewItem.ListViewSubItem(item, sampleRate.ToString() + "kHz"));
                    }
                    else
                        item.SubItems.Add(new ListViewItem.ListViewSubItem(item, vl.SampleRate.ToString() + "Hz"));*/
                
                
                label = new Label();
                label.AutoSize = true;
                label.Parent = this;
                if (fVoiceLog.SampleRate >= 1000)
                {
                    double sampleRate = fVoiceLog.SampleRate / 1000;
                    label.Text = sampleRate.ToString() + "kHz";
                }
                else
                    label.Text = fVoiceLog.SampleRate.ToString() + "Hz";                
                tableIndividual.Controls.Add(label, 1, 3);

                label = new Label();
                label.AutoSize = true;
                label.Parent = this;
                label.Text = "Duration";
                label.Font = new System.Drawing.Font(this.Font.FontFamily, this.Font.Size, FontStyle.Bold);
                tableIndividual.Controls.Add(label, 2, 2);

                label = new Label();
                label.AutoSize = true;
                label.Parent = this;
                label.Text = fVoiceLog.FileDuration.ToString(@"hh\:mm\:ss\.fff");
                tableIndividual.Controls.Add(label, 2, 3);

                coChartIndividual.SelectedIndex = 0;
                LoadIndividualChart();
                LoadIndividualCloud();
                LoadCompliance();
            }
        }

        private void LoadIndividualCloud()
        {
            if (cloudIndividual.WeightedWords != null)
                cloudIndividual.WeightedWords.Clear();
            cloudIndividual.Refresh();
            if (fVoiceLog != null)
            {
                Language language = ByLanguageFactory.GetLanguageFromString("English *.txt");
                FileIterator fileIterator = ByLanguageFactory.GetFileIterator(language);

                IBlacklist blacklist = ByLanguageFactory.GetBlacklist(language);
                IWordStemmer stemmer = ByLanguageFactory.GetStemmer(language);

                cloudIndividual.WeightedWords = new List<IWord>();
                fCancelSourceIndividual = new CancellationTokenSource();
                Task.Factory
                    .StartNew(
                        () => GetWordsParallelyIndividual(language, blacklist, stemmer), fCancelSourceIndividual.Token)
                    .ContinueWith(
                        ApplyIndividualResults);
            }
        }

        private void listVoiceLogs_Click(object sender, EventArgs e)
        {
            if ((fVoiceLogs != null) &&
                (fVoiceLogs.Count > 0) &&
                (listVoiceLogs.Items.Count > 0) &&
                (listVoiceLogs.SelectedItems.Count > 0))
            {
                fVoiceLog = fVoiceLogs[listVoiceLogs.SelectedItems[0].Index];
            }
            if (fVoiceLog != null)
            {
                tabPages.SelectedTab = tabDetail;
                LoadIndividual();
            }
        }

        private void ApplyIndividualResults(Task<List<IWord>> task)
        {
            this.Invoke(
                new Action(
                    () =>
                    {
                        if (task.IsCanceled)
                        {
                            cloudIndividual.WeightedWords = new List<IWord>();
                        }
                        else
                        {
                            if (task.IsFaulted && task.Exception != null)
                            {
                                throw task.Exception;
                            }
                            
                            cloudIndividual.WeightedWords = task.Result;
                            
                        }

                        fTotalWordCountIndividual = cloudIndividual.WeightedWords.Sum(word => word.Occurrences);
                    }));
        }

        private List<IWord> GetWordsParallelyIndividual(Language language, IBlacklist blacklist, IWordStemmer stemmer)
        {
            string sentiment = string.Empty;
            sentiment += fVoiceLog.Sentiment.NegativeWords + fVoiceLog.Sentiment.PositiveWords;

            string[] keywords = ExtractKeywords(sentiment);

            return keywords
                .AsParallel()
                .WithCancellation(fCancelSourceIndividual.Token)
                .Filter(blacklist)
                .CountOccurences()
                .GroupByStem(stemmer)
                .SortByOccurences()
                .AsEnumerable()
                .Cast<IWord>()
                .ToList();
        }

        private void coChartIndividual_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadIndividualChart();
        }

        public void LoadIndividualChart()
        {
            if (fVoiceLog!=null)
            {
                chartIndividual.Series.Clear();

                float negative = 0;
                float neutral = 0;
                float positive = 0;


                negative += fVoiceLog.Sentiment.Negative;
                neutral += fVoiceLog.Sentiment.Neutral;
                positive += fVoiceLog.Sentiment.Positive;
                

                if (coChartIndividual.SelectedIndex == 0)
                {
                    System.Windows.Forms.DataVisualization.Charting.Series series = chartIndividual.Series.Add("Sentiment");
                    chartIndividual.Series["Sentiment"].IsVisibleInLegend = false;
                    chartIndividual.Series["Sentiment"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
                    chartIndividual.Series["Sentiment"].SmartLabelStyle.AllowOutsidePlotArea = System.Windows.Forms.DataVisualization.Charting.LabelOutsidePlotAreaStyle.Yes;

                    System.Windows.Forms.DataVisualization.Charting.DataPoint dp;
                    dp = chartIndividual.Series["Sentiment"].Points.Add(negative);
                    dp.Label = "Negative - " + negative.ToString();
                    dp.LabelForeColor = Color.White;

                    dp = chartIndividual.Series["Sentiment"].Points.Add(neutral);
                    dp.Label = "Neutral - " + neutral.ToString();
                    dp.LabelForeColor = Color.White;

                    dp = chartIndividual.Series["Sentiment"].Points.Add(positive);
                    dp.Label = "Positive - " + positive.ToString();
                    dp.LabelForeColor = Color.White;

                    chartIndividual.Series["Sentiment"].ChartArea = "ChartArea1";
                    chartIndividual.Series["Sentiment"].Font = new System.Drawing.Font(this.Font.FontFamily, this.Font.Size, FontStyle.Regular);
                    //chartSentiment.Legends[0].Font = new System.Drawing.Font(this.Font.FontFamily, this.Font.Size, FontStyle.Regular);
                }

                if (coChartIndividual.SelectedIndex == 1)
                {
                    System.Windows.Forms.DataVisualization.Charting.Series series = chartIndividual.Series.Add("Sentiment");
                    chartIndividual.Series["Sentiment"].IsVisibleInLegend = false;
                    chartIndividual.Series["Sentiment"].Color = Color.SteelBlue;
                    chartIndividual.Series["Sentiment"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;

                    System.Windows.Forms.DataVisualization.Charting.DataPoint dp;
                    dp = chartIndividual.Series["Sentiment"].Points.Add(negative);
                    dp.Label = "Negative - " + negative.ToString();

                    dp = chartIndividual.Series["Sentiment"].Points.Add(neutral);
                    dp.Label = "Neutral - " + neutral.ToString();

                    dp = chartIndividual.Series["Sentiment"].Points.Add(positive);
                    dp.Label = "Positive - " + positive.ToString();

                    
                    
                    chartIndividual.Series["Sentiment"].ChartArea = "ChartArea1";
                    chartIndividual.Series["Sentiment"].Font = new System.Drawing.Font(this.Font.FontFamily, this.Font.Size, FontStyle.Regular);

                    chartIndividual.ChartAreas["ChartArea1"].AxisX.LabelStyle.Font = new System.Drawing.Font(this.Font.FontFamily, this.Font.Size, FontStyle.Regular);
                    chartIndividual.ChartAreas["ChartArea1"].AxisY.LabelStyle.Font = new System.Drawing.Font(this.Font.FontFamily, this.Font.Size, FontStyle.Regular);

                    chartIndividual.ChartAreas["ChartArea1"].AxisY.Title = "Sentiment";
                }
            }
        }

        public void LoadCompliance()
        {
            listCompliance.Items.Clear();

            if ((fVoiceLog != null) && (fVoiceLog.Compliance != null))
            {
                ListViewItem item;
                
                item = new ListViewItem();
                item.Text = "Appropriate greeting";
                item.ImageIndex = Convert.ToInt32(fVoiceLog.Compliance.Greeting);
                listCompliance.Items.Add(item);

                item = new ListViewItem();
                item.Text = "Disclaimer read";
                item.ImageIndex = Convert.ToInt32(fVoiceLog.Compliance.DisclaimerRead);
                listCompliance.Items.Add(item);

                item = new ListViewItem();
                item.Text = "Disclaimer accepted";
                item.ImageIndex = Convert.ToInt32(fVoiceLog.Compliance.DisclaimerAccepted);
                listCompliance.Items.Add(item);

                item = new ListViewItem();
                item.Text = "No profanity detected";
                if (fVoiceLog.Compliance.Profanity == true)
                    item.ImageIndex = 0;
                else
                    item.ImageIndex = 1;
                listCompliance.Items.Add(item);
            }

            
        }

        public void GetAudioDevices()
        {
            comboDevices.Items.Clear();
            int devices = WaveIn.DeviceCount;
            for (int i = 0; i < devices; i++)
            {
                WaveInCapabilities deviceInfo = WaveIn.GetCapabilities(i);
                comboDevices.Items.Add(deviceInfo.ProductName);
            }
            if (devices > 0)
                comboDevices.SelectedIndex = 0;
        }

        private void toolStripButton1_Click_1(object sender, EventArgs e)
        {
            for (int i = 0; i < 3; i++)
                listViewEvents.Items[i].ImageIndex = 0;

            textSampleLog.AppendText("Recording new voice sample\n");

            Guid voxId = Guid.NewGuid();
            textSampleId.Text = voxId.ToString();
            
            int deviceNumber = comboDevices.SelectedIndex;
            if (!System.IO.Directory.Exists(@"c:\temp"))
                System.IO.Directory.CreateDirectory(@"c:\temp");

            string saveLocation = "c:\\temp\\" + textSampleId.Text.Trim() + ".wav";

            fSourceStream = new WaveIn();
            fSourceStream.DeviceNumber = deviceNumber;
            fSourceStream.WaveFormat = new WaveFormat(16000, 1);
            fWaveWriter = new WaveFileWriter(saveLocation, fSourceStream.WaveFormat);

            fSourceStream.DataAvailable += fSourceStream_DataAvailable;

            fSourceStream.StartRecording();
        }

        void fSourceStream_DataAvailable(object sender, WaveInEventArgs e)
        {
            if (fWaveWriter != null)
            {
                fWaveWriter.Write(e.Buffer, 0, e.BytesRecorded);
                fWaveWriter.Flush();
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            fSourceStream.StopRecording();
            fWaveWriter.Close();

            textSampleLog.AppendText("Voice recording stopped" + Environment.NewLine);

            while (!System.IO.File.Exists(@"c:\temp\" + textSampleId.Text + ".wav"))
                System.Threading.Thread.Sleep(100);
            if (System.IO.File.Exists(@"c:\temp\" + textSampleId.Text + ".wav"))
            {
                WebClient ftpClient = new WebClient();
                try
                {
                    textSampleLog.AppendText("Uploading file to 'ftp://" + textFtpServer.Text.Trim() + "/" + textFtpDir.Text.Trim() + "/" + textSampleId.Text + ".wav'" + Environment.NewLine);
                    ftpClient.Credentials = new NetworkCredential(textFtpUser.Text.Trim(), textFtpPass.Text.Trim());
                    ftpClient.UploadFile("ftp://" + textFtpServer.Text.Trim() + "/" + textFtpDir.Text.Trim() + "/" + textSampleId.Text + ".wav", @"c:\temp\" + textSampleId.Text + ".wav");
                    textSampleLog.AppendText("Upload of email transcript complete" + Environment.NewLine);
                }
                catch (Exception exc)
                {
                    textSampleLog.AppendText("Error uploading audio file: " + exc.Message + Environment.NewLine);
                }
                finally
                {
                    ftpClient.Dispose();
                }

                //FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://" + textFtpServer.Text.Trim() + "/" + textFtpDir.Text.Trim() + "/" + textSampleId.Text + ".wav");
                //request.UseBinary = true;
                //request.UsePassive = false;
                //request.Method = WebRequestMethods.Ftp.UploadFile;
                //request.Credentials = new NetworkCredential(textFtpUser.Text.Trim(), textFtpPass.Text.Trim());
                //System.IO.StreamReader sourceStream = new System.IO.StreamReader(@"c:\temp\" + textSampleId.Text + ".wav");
                //byte[] fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
                //sourceStream.Close();
                //request.ContentLength = fileContents.Length;
                //System.IO.Stream requestStream = request.GetRequestStream();
                //requestStream.Write(fileContents, 0, fileContents.Length);
                //requestStream.Close();

                //FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                //response.Close();



                /*
                 // Get the object used to communicate with the server.
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://www.contoso.com/test.htm");
                request.Method = WebRequestMethods.Ftp.UploadFile;

                // This example assumes the FTP site uses anonymous logon.
                request.Credentials = new NetworkCredential ("anonymous","janeDoe@contoso.com");

                // Copy the contents of the file to the request stream.
                StreamReader sourceStream = new StreamReader("testfile.txt");
                byte [] fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
                sourceStream.Close();
                request.ContentLength = fileContents.Length;

                Stream requestStream = request.GetRequestStream();
                requestStream.Write(fileContents, 0, fileContents.Length);
                requestStream.Close();

                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                response.Close();
                 */

            }
        }

        private void buttonLoadLogs_Click(object sender, EventArgs e)
        {
            //LoadLogs(MetaFileType.All);
        }

        private void LoadLogs(MetaFileType AType)
        {
            fVoiceLog = null;
            MySqlConnection conn = new MySqlConnection(Properties.Settings.Default.db.ToString());
            try
            {
                if (fVoiceLogs == null)
                    fVoiceLogs = new VoiceLogs();
                fVoiceLogs.Clear();
                if (fVoiceLogs.Load(conn, Properties.Settings.Default.db.ToString(), AType, string.Empty))
                {
                    if (cloudControl.WeightedWords != null)
                        cloudControl.WeightedWords.Clear();
                    listVoiceLogs.Items.Clear();
                }
            }
            catch (Exception exc)
            {
                teDatabaseLog.AppendText("Failure loading voice logs: " + exc.Message + "\n");
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                    conn.Close();
                conn.Dispose();
            }
            BuildSummary();            
        }

        private void LoadLog(string AId)
        {
            //  clear log
            fVoiceLog = null;
            MySqlConnection conn = new MySqlConnection(Properties.Settings.Default.db.ToString());
            try
            {
                if (fVoiceLogs == null)
                    fVoiceLogs = new VoiceLogs();
                fVoiceLogs.Clear();
                if (fVoiceLogs.Load(conn, Properties.Settings.Default.db.ToString(), MetaFileType.All, AId))
                {
                    if (cloudControl.WeightedWords != null)
                        cloudControl.WeightedWords.Clear();
                    listVoiceLogs.Items.Clear();
                }
            }
            catch (Exception exc)
            {
                teDatabaseLog.AppendText("Failure loading voice logs: " + exc.Message + "\n");
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                    conn.Close();
                conn.Dispose();
            }
            BuildSummary();
            if (fVoiceLogs.Count > 0)
            {
                fVoiceLog = fVoiceLogs[0];
                if (fVoiceLog != null)
                {
                    tabPages.SelectedTab = tabDetail;
                    LoadIndividual();
                }
            }


            //  load id
            //  build summary
            //  log log
            //  switch display
        }

        private void allToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fMeta = MetaFileType.All;
            LoadLogs(fMeta);
        }

        private void voiceLogsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fMeta = MetaFileType.Voice;
            LoadLogs(fMeta);
        }

        private void emailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fMeta = MetaFileType.Mail;
            LoadLogs(fMeta);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.ftpserver = textFtpServer.Text.Trim();
            Properties.Settings.Default.ftpuser = textFtpUser.Text.Trim();
            Properties.Settings.Default.ftppassword = textFtpPass.Text.Trim();
            Properties.Settings.Default.ftpremotedir = textFtpDir.Text.Trim();
            Properties.Settings.Default.Save();
        }

        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            Guid id = Guid.NewGuid();
            textSampleId.Text = id.ToString();

            for (int i = 0; i < 3; i++)
                listViewEvents.Items[i].ImageIndex = 0;

            if (openAudio.ShowDialog() == DialogResult.OK)
            {
                textSampleLog.AppendText("Uploading WAV file to FTP server: ftp://" + textFtpServer.Text.Trim() + "/" + textFtpDir.Text.Trim() + "/" + textSampleId.Text + ".wax" + Environment.NewLine);
                WebClient ftpClient = new WebClient();
                //FtpWebRequest request = null;
                try
                {
                    //  upload file
                    ftpClient.Credentials = new NetworkCredential(textFtpUser.Text.Trim(), textFtpPass.Text.Trim());
                    ftpClient.UploadFile("ftp://" + textFtpServer.Text.Trim() + "/" + textFtpDir.Text.Trim() + "/" + textSampleId.Text + ".wax", openAudio.FileName);
                    textSampleLog.AppendText("Upload of email transcript complete" + Environment.NewLine);

                    //request = (FtpWebRequest)WebRequest.Create("ftp://" + textFtpServer.Text.Trim() + "/" + textFtpDir.Text.Trim() + "/" + textSampleId.Text + ".wax");
                    //request.UseBinary = true;
                    //request.UsePassive = false;
                    //request.Method = WebRequestMethods.Ftp.UploadFile;
                    //request.Credentials = new NetworkCredential(textFtpUser.Text.Trim(), textFtpPass.Text.Trim());
                    //System.IO.StreamReader sourceStream = new System.IO.StreamReader(openAudio.FileName);
                    //byte[] fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
                    //sourceStream.Close();
                    //request.ContentLength = fileContents.Length;
                    
                    
                    
                    //System.IO.Stream requestStream = request.GetRequestStream();
                    //requestStream.Write(fileContents, 0, fileContents.Length);
                    //requestStream.Close();

                    //FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                    //response.Close();

                    Thread.Sleep(1000);

                    //rename file
                    FtpWebRequest requestFTP;
                    requestFTP = (FtpWebRequest)FtpWebRequest.Create("ftp://" + textFtpServer.Text.Trim() + "/" + textFtpDir.Text.Trim() + "/" + textSampleId.Text + ".wax");                    
                    requestFTP.Credentials = new NetworkCredential(textFtpUser.Text.Trim(), textFtpPass.Text.Trim());

                    //string newFilename = "ftp://" + textFtpServer.Text.Trim() + "/" + textFtpDir.Text.Trim() + "/" + textSampleId.Text + ".wav";
                    string newFilename = textSampleId.Text + ".wav";

                    requestFTP.Method = WebRequestMethods.Ftp.Rename;
                    requestFTP.RenameTo = newFilename;
                    requestFTP.GetResponse();
                   
                    textSampleLog.AppendText("Rename of transcript file complete" + Environment.NewLine);
                }
                catch (Exception exc)
                {
                    textSampleLog.AppendText("Error uploading transcript: " + exc.Message + Environment.NewLine);
                }
                finally
                {
                    //request = null;
                    ftpClient.Dispose();
                }

            }
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 3; i++)
                listViewEvents.Items[i].ImageIndex = 0;

            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(textSMTP.Text.Trim());
            try
            {
                System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
                message.Subject = Guid.NewGuid().ToString();
                textSampleId.Text = message.Subject;
                message.Sender = new System.Net.Mail.MailAddress("aidemo@pyro.dariel.co.za");
                message.From = new System.Net.Mail.MailAddress("aidemo@pyro.dariel.co.za");
                message.To.Add("aidemo@pyro.dariel.co.za");
                message.Body = "Out of the night that covers me, " +
                        "Black as the pit from pole to pole, " +
                        "I thank whatever gods may be " +
                        "For my unconquerable soul. " +
                        " " +
                        "In the fell clutch of circumstance " +
                        "I have not winced nor cried aloud. " +
                        "Under the bludgeonings of chance " +
                        "My head is bloody, but unbowed. " +
                        " " +
                        "Beyond this place of wrath and tears " +
                        "Looms but the Horror of the shade, " +
                        "And yet the menace of the years " +
                        "Finds, and shall find me, unafraid. " +
                        " " +
                        "It matters not how strait the gate, " +
                        "How charged with punishments the scroll, " +
                        "I am the master of my fate: " +
                        "I am the captain of my soul. ";
                smtp.Send(message);
                textSampleLog.AppendText("Sample mail sent.  Awating monitoring response for " + message.Subject + "\n");
            }
            finally
            {
                smtp.Dispose();
            }
        }

        private void buttonLoadLogs_ButtonClick(object sender, EventArgs e)
        {
            
        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            for (int i = 1; i < 21; i++)
            {
                string textFile = System.IO.File.ReadAllText(@"C:\Temp\ai\email inputs\" + i.ToString() + ".txt");
                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(textSMTP.Text.Trim());
                System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
                try
                {
                    Guid id = Guid.NewGuid();
                    message.Subject = id.ToString();
                    message.Sender = new System.Net.Mail.MailAddress("aidemo@pyro.dariel.co.za");
                    message.From = new System.Net.Mail.MailAddress("aidemo@pyro.dariel.co.za");
                    message.To.Add("aidemo@pyro.dariel.co.za");
                    message.Body = textFile;
                    smtp.Send(message);
                    textSampleLog.AppendText("Sample mail sent: " + i.ToString() + "\n");
                    System.IO.File.WriteAllText(@"C:\Temp\ai\email inputs\" + i.ToString() + "-" + id.ToString() + ".txt", textFile);
                }
                catch (Exception exc)
                {
                    textSampleLog.AppendText("Failure to send email " + i.ToString() + " - " + exc.Message);
                }
                finally
                {
                    message.Dispose();
                    smtp.Dispose();
                }
            }

        }

        private void buttonTCPConnect_Click(object sender, EventArgs e)
        {
            if (fSocket == null)
            {
                fSocket = new TCPSocket();
                fSocket.OnConnected += fSocket_OnConnected;
                fSocket.OnDisconnected += fSocket_OnDisconnected;
                fSocket.OnError += fSocket_OnError;
                fSocket.OnMessage += fSocket_OnMessage;
            }
            else
                if (fSocket.IsConnected)
                {
                    return;
                }
            if ((fSocket != null) && (!fSocket.IsConnected))
            {                
                fSocket.Address = textTCPServer.Text.Trim();
                fSocket.Port = Convert.ToInt32(textTCPPort.Text.Trim());
                teUDPLog.AppendText("Connecting to TCP server " + fSocket.Address + ":" + fSocket.Port.ToString() + Environment.NewLine);
                fSocket.Connect();
            }
        }


        /*delegate void ErrorCallback(object ASender, string AMessage);
        void udp_OnError(object ASender, string AMessage)
        {
            if (this.teUDPLog.InvokeRequired)
            {
                ErrorCallback e = new ErrorCallback(udp_OnError);
                this.Invoke(e, new object[] { ASender, AMessage });
            }
            else
                teUDPLog.AppendText("UDP error - " + AMessage + "\n");
        }*/


        delegate void TcpSocketOnMessageCallback(object ASender, string AMessage);
        void fSocket_OnMessage(object ASender, string AMessage)
        {
            if (this.teUDPLog.InvokeRequired)
            {
                TcpSocketOnMessageCallback c = new TcpSocketOnMessageCallback(fSocket_OnMessage);
                this.Invoke(c, new object[] { ASender, AMessage });
            }
            else
            {
                if (AMessage.Substring(0, 10) != "KEEP-ALIVE")
                {                    
                    BroadcastMessage bcm = new BroadcastMessage();
                    if (bcm.Read(AMessage))
                    {
                        teUDPLog.AppendText(" - TxID: " + bcm.TransactionId.ToString() + "\n");
                        teUDPLog.AppendText(" - Timestamp: " + bcm.TimeStamp.ToString() + "\n");
                        teUDPLog.AppendText(" - Process type: " + bcm.ProcessType + "\n");
                        teUDPLog.AppendText(" - Error code: " + bcm.ErrorCode + "\n");
                        teUDPLog.AppendText(" - Message: " + bcm.Message + "\n");
                        teUDPLog.AppendText(System.Environment.NewLine + System.Environment.NewLine);

                        toolStripButton1_Click(this, null);

                        if (bcm.TransactionId.ToString() == textSampleId.Text)
                        {
                            if (bcm.ProcessType == "V2T") listViewEvents.Items[0].ImageIndex = 1;
                            if (bcm.ProcessType == "SENT") listViewEvents.Items[1].ImageIndex = 1;
                            if (bcm.ProcessType == "COMP") listViewEvents.Items[2].ImageIndex = 1;
                        }
                    }
                    else
                    {
                        teUDPLog.AppendText(" - Error reading BCM: " + bcm.ReadError + "\n");
                    }
                }
                else
                {
                    //teUDPLog.AppendText("Keep-alive received from server.  Remove when done..." + AMessage + Environment.NewLine);
                }

                //if (this.teUDPLog.InvokeRequired)
                //{
                //    MessageReceivedCallback m = new MessageReceivedCallback(udp_OnMessageReceived);
                //    this.Invoke(m, new object[] { ASender, AIP, AMessage, ALen });
                //}
                //else
                //{
                //    teUDPLog.AppendText("Message received: (" + AIP.ToString() + ") [" + ALen.ToString() + " bytes]\n");
                //    teUDPLog.AppendText(System.Environment.NewLine);
                //    teUDPLog.AppendText(AMessage);
                //    teUDPLog.AppendText(System.Environment.NewLine);
                //    BroadcastMessage bcm = new BroadcastMessage();
                //    if (bcm.Read(AMessage))
                //    {
                //        teUDPLog.AppendText(" - TxID: " + bcm.TransactionId.ToString() + "\n");
                //        teUDPLog.AppendText(" - Timestamp: " + bcm.TimeStamp.ToString() + "\n");
                //        teUDPLog.AppendText(" - Process type: " + bcm.ProcessType + "\n");
                //        teUDPLog.AppendText(" - Error code: " + bcm.ErrorCode + "\n");
                //        teUDPLog.AppendText(" - Message: " + bcm.Message + "\n");
                //        teUDPLog.AppendText(System.Environment.NewLine);

                //        toolStripButton1_Click(this, null);

                //        if (bcm.TransactionId.ToString() == textSampleId.Text)
                //        {
                //            if (bcm.ProcessType == "V2T") listViewEvents.Items[0].ImageIndex = 1;
                //            if (bcm.ProcessType == "SENT") listViewEvents.Items[1].ImageIndex = 1;
                //            if (bcm.ProcessType == "COMP") listViewEvents.Items[2].ImageIndex = 1;
                //        }
                //    }
                //    else
                //    {
                //        teUDPLog.AppendText(" - Error reading BCM: " + bcm.ReadError + "\n");
                //    }
                //}
                //teUDPLog.AppendText("TCP message received: " + AMessage + Environment.NewLine);

            }
        }

        delegate void TcpSocketOnErrorCallback(object ASender, string AMessage);
        void fSocket_OnError(object ASender, string AErrorMessage)
        {
            if (this.teUDPLog.InvokeRequired)
            {
                TcpSocketOnErrorCallback c = new TcpSocketOnErrorCallback(fSocket_OnError);
                this.Invoke(c, new object[] { ASender, AErrorMessage });
            }
            else
                teUDPLog.AppendText("TCP error: " + AErrorMessage + Environment.NewLine);
        }

        delegate void TcpSocketOnDisconnectedCallback(object ASender);
        void fSocket_OnDisconnected(object ASender)
        {
            if (this.teUDPLog.InvokeRequired)
            {
                TcpSocketOnDisconnectedCallback c = new TcpSocketOnDisconnectedCallback(fSocket_OnDisconnected);
                this.Invoke(c, new object[] { ASender });
            }
            else
                teUDPLog.AppendText("TCP client disconnected" + Environment.NewLine);
        }

        delegate void TcpSocketOnConnectedCallback(object ASender);
        void fSocket_OnConnected(object ASender)
        {
            if (this.teUDPLog.InvokeRequired)
            {
                TcpSocketOnConnectedCallback c = new TcpSocketOnConnectedCallback(fSocket_OnConnected);
                this.Invoke(c, new object[] { ASender });
            }
            else
                teUDPLog.AppendText("TCP client connected" + Environment.NewLine);
        }

        private void buttonTCPClose_Click(object sender, EventArgs e)
        {
            if (fSocket.IsConnected)
            {
                fSocket.Close();
            }
        }

        private void buttonClearUdpLog_Click(object sender, EventArgs e)
        {
            teUDPLog.Clear();
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            if (textSampleId.Text != string.Empty)
            {
                LoadLog(textSampleId.Text);
            }
        }

        
    }
}
