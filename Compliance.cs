﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace AIdemo
{
    public class Compliance
    {
        string _transactionId = string.Empty;
        bool _greeting = false;
        bool _disclaimerRead = false;
        bool _disclaimerAccepted = false;
        bool _profanity = false;
        string _error = string.Empty;

        public string TransactionId { get { return _transactionId; } set { _transactionId = value; } }
        public bool Greeting { get { return _greeting; } set { _greeting = value; } }
        public bool DisclaimerRead { get { return _disclaimerRead; } set { _disclaimerRead = value; } }
        public bool DisclaimerAccepted { get { return _disclaimerAccepted; } set { _disclaimerAccepted = value; } }
        public bool Profanity { get { return _profanity; } set { _profanity = value; } }
        public string Error { get { return _error; } }

        private Compliance() { }

        public Compliance(string AId)
        {
            _transactionId = AId;
        }

        public bool Load(MySqlConnection AConnection)
        {
            bool result = false;
            try
            {
                if (AConnection != null)
                {
                    if (AConnection.State != System.Data.ConnectionState.Open)
                        AConnection.Open();
                    if (AConnection.State == System.Data.ConnectionState.Open)
                    {
                        MySqlCommand comm = new MySqlCommand("SELECT * FROM compliance WHERE UUID = @Id", AConnection);
                        comm.Parameters.AddWithValue("Id", _transactionId);
                        MySqlDataReader reader = comm.ExecuteReader();
                        if (reader.HasRows)
                        {
                            if (reader.Read())
                            {
                                result = this.Read(reader);
                            }
                        }
                        comm.Dispose();
                        reader.Close();
                        reader.Dispose();

                    }
                }
            }
            catch (Exception exc) { _error = exc.Message; }
            return result;
        }

        public bool Read(MySqlDataReader AReader)
        {
            bool result = false;
            try
            {
                if (AReader != null)
                {
                    _transactionId = AReader.GetString(AReader.GetOrdinal("UUID"));
                    _greeting = AReader.GetBoolean(AReader.GetOrdinal("Greeting"));
                    _disclaimerRead = AReader.GetBoolean(AReader.GetOrdinal("Disclaimer_read"));
                    _disclaimerAccepted = AReader.GetBoolean(AReader.GetOrdinal("Disclaimer_accpeted"));
                    _profanity = AReader.GetBoolean(AReader.GetOrdinal("Profanity"));

                    result = true;
                }
            }
            catch (Exception exc) { _error = exc.Message; }
            return result;
        }
    }

}
