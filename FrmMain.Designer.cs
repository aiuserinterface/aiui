﻿namespace AIdemo
{
    partial class formMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formMain));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem("Voice to text", 0);
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem("Sentiment", 0);
            System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem("Compliance", 0);
            this.tabPages = new System.Windows.Forms.TabControl();
            this.tabSummary = new System.Windows.Forms.TabPage();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.buttonLoadLogs = new System.Windows.Forms.ToolStripSplitButton();
            this.allToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.voiceLogsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.emailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toRecord = new System.Windows.Forms.ToolStripButton();
            this.toStop = new System.Windows.Forms.ToolStripButton();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.coChart = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.chartSentiment = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label3 = new System.Windows.Forms.Label();
            this.listVoiceLogs = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.imageListType = new System.Windows.Forms.ImageList(this.components);
            this.tabDetail = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.chartIndividual = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.coChartIndividual = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.listCompliance = new System.Windows.Forms.ListView();
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.imageListTrueFalse = new System.Windows.Forms.ImageList(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.tableIndividual = new System.Windows.Forms.TableLayoutPanel();
            this.tabNetworking = new System.Windows.Forms.TabPage();
            this.buttonTCPClose = new System.Windows.Forms.Button();
            this.textTCPPort = new System.Windows.Forms.TextBox();
            this.textTCPServer = new System.Windows.Forms.TextBox();
            this.buttonTCPConnect = new System.Windows.Forms.Button();
            this.textSMTP = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.buClearLog = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.teUDPLog = new System.Windows.Forms.TextBox();
            this.buUDPSend = new System.Windows.Forms.Button();
            this.tePort = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buUDPListen = new System.Windows.Forms.Button();
            this.tabVoiceRecorder = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.textFtpDir = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.textFtpPass = new System.Windows.Forms.TextBox();
            this.textFtpUser = new System.Windows.Forms.TextBox();
            this.textFtpServer = new System.Windows.Forms.TextBox();
            this.listViewEvents = new System.Windows.Forms.ListView();
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.imageListEvents = new System.Windows.Forms.ImageList(this.components);
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.textSampleId = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel4 = new System.Windows.Forms.Panel();
            this.textSampleLog = new System.Windows.Forms.TextBox();
            this.comboDevices = new System.Windows.Forms.ComboBox();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.tabDatabase = new System.Windows.Forms.TabPage();
            this.teDatabaseLog = new System.Windows.Forms.TextBox();
            this.teConnectionString = new System.Windows.Forms.TextBox();
            this.buConnectDB = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.openAudio = new System.Windows.Forms.OpenFileDialog();
            this.buttonClearUdpLog = new System.Windows.Forms.Button();
            this.cloudControl = new Gma.CodeCloud.Controls.CloudControl();
            this.cloudIndividual = new Gma.CodeCloud.Controls.CloudControl();
            this.button3 = new System.Windows.Forms.Button();
            this.tabPages.SuspendLayout();
            this.tabSummary.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartSentiment)).BeginInit();
            this.tabDetail.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartIndividual)).BeginInit();
            this.tabNetworking.SuspendLayout();
            this.tabVoiceRecorder.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.tabDatabase.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabPages
            // 
            this.tabPages.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabPages.Controls.Add(this.tabSummary);
            this.tabPages.Controls.Add(this.tabDetail);
            this.tabPages.Controls.Add(this.tabNetworking);
            this.tabPages.Controls.Add(this.tabVoiceRecorder);
            this.tabPages.Controls.Add(this.tabDatabase);
            this.tabPages.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tabPages.Location = new System.Drawing.Point(17, 18);
            this.tabPages.Margin = new System.Windows.Forms.Padding(12);
            this.tabPages.Name = "tabPages";
            this.tabPages.SelectedIndex = 0;
            this.tabPages.Size = new System.Drawing.Size(973, 615);
            this.tabPages.TabIndex = 0;
            this.tabPages.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabSummary
            // 
            this.tabSummary.Controls.Add(this.toolStrip1);
            this.tabSummary.Controls.Add(this.splitContainer1);
            this.tabSummary.Controls.Add(this.listVoiceLogs);
            this.tabSummary.Location = new System.Drawing.Point(4, 25);
            this.tabSummary.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabSummary.Name = "tabSummary";
            this.tabSummary.Padding = new System.Windows.Forms.Padding(12);
            this.tabSummary.Size = new System.Drawing.Size(965, 586);
            this.tabSummary.TabIndex = 0;
            this.tabSummary.Text = "Summary";
            this.tabSummary.UseVisualStyleBackColor = true;
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buttonLoadLogs,
            this.toolStripSeparator1,
            this.toolStripButton3,
            this.toolStripButton4,
            this.toolStripSeparator2,
            this.toRecord,
            this.toStop});
            this.toolStrip1.Location = new System.Drawing.Point(12, 12);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(2);
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStrip1.Size = new System.Drawing.Size(941, 43);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // buttonLoadLogs
            // 
            this.buttonLoadLogs.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonLoadLogs.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.allToolStripMenuItem,
            this.voiceLogsToolStripMenuItem,
            this.emailsToolStripMenuItem});
            this.buttonLoadLogs.Image = ((System.Drawing.Image)(resources.GetObject("buttonLoadLogs.Image")));
            this.buttonLoadLogs.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonLoadLogs.Name = "buttonLoadLogs";
            this.buttonLoadLogs.Size = new System.Drawing.Size(48, 36);
            this.buttonLoadLogs.Text = "toolStripSplitButton1";
            this.buttonLoadLogs.ToolTipText = "Load logs";
            this.buttonLoadLogs.ButtonClick += new System.EventHandler(this.buttonLoadLogs_ButtonClick);
            this.buttonLoadLogs.Click += new System.EventHandler(this.buttonLoadLogs_Click);
            // 
            // allToolStripMenuItem
            // 
            this.allToolStripMenuItem.Name = "allToolStripMenuItem";
            this.allToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.allToolStripMenuItem.Text = "All";
            this.allToolStripMenuItem.Click += new System.EventHandler(this.allToolStripMenuItem_Click);
            // 
            // voiceLogsToolStripMenuItem
            // 
            this.voiceLogsToolStripMenuItem.Name = "voiceLogsToolStripMenuItem";
            this.voiceLogsToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.voiceLogsToolStripMenuItem.Text = "Voice logs";
            this.voiceLogsToolStripMenuItem.Click += new System.EventHandler(this.voiceLogsToolStripMenuItem_Click);
            // 
            // emailsToolStripMenuItem
            // 
            this.emailsToolStripMenuItem.Name = "emailsToolStripMenuItem";
            this.emailsToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.emailsToolStripMenuItem.Text = "Emails";
            this.emailsToolStripMenuItem.Click += new System.EventHandler(this.emailsToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton3.Text = "toolStripButton3";
            this.toolStripButton3.ToolTipText = "Switch to demo mode";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton4.Text = "toolStripButton4";
            this.toolStripButton4.ToolTipText = "Switch to debug mode";
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 39);
            // 
            // toRecord
            // 
            this.toRecord.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toRecord.Image = ((System.Drawing.Image)(resources.GetObject("toRecord.Image")));
            this.toRecord.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toRecord.Name = "toRecord";
            this.toRecord.Size = new System.Drawing.Size(36, 36);
            this.toRecord.Text = "toolStripButton1";
            this.toRecord.ToolTipText = "Record a conversation now";
            // 
            // toStop
            // 
            this.toStop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toStop.Enabled = false;
            this.toStop.Image = ((System.Drawing.Image)(resources.GetObject("toStop.Image")));
            this.toStop.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toStop.Name = "toStop";
            this.toStop.Size = new System.Drawing.Size(36, 36);
            this.toStop.Text = "toolStripButton2";
            this.toStop.ToolTipText = "Stop recording";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(16, 244);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.coChart);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.chartSentiment);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.cloudControl);
            this.splitContainer1.Panel2.Controls.Add(this.label3);
            this.splitContainer1.Size = new System.Drawing.Size(967, 319);
            this.splitContainer1.SplitterDistance = 471;
            this.splitContainer1.SplitterWidth = 5;
            this.splitContainer1.TabIndex = 1;
            // 
            // coChart
            // 
            this.coChart.Dock = System.Windows.Forms.DockStyle.Top;
            this.coChart.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.coChart.FormattingEnabled = true;
            this.coChart.Items.AddRange(new object[] {
            "Pie chart",
            "Bar chart"});
            this.coChart.Location = new System.Drawing.Point(0, 37);
            this.coChart.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.coChart.Name = "coChart";
            this.coChart.Size = new System.Drawing.Size(471, 24);
            this.coChart.TabIndex = 2;
            this.coChart.SelectedIndexChanged += new System.EventHandler(this.coChart_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.label2.Size = new System.Drawing.Size(471, 37);
            this.label2.TabIndex = 3;
            this.label2.Text = "Prevailing Sentiment";
            // 
            // chartSentiment
            // 
            this.chartSentiment.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea1.AxisY.TitleFont = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea1.Name = "ChartArea1";
            this.chartSentiment.ChartAreas.Add(chartArea1);
            this.chartSentiment.Location = new System.Drawing.Point(3, 74);
            this.chartSentiment.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chartSentiment.Name = "chartSentiment";
            this.chartSentiment.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Bright;
            this.chartSentiment.Size = new System.Drawing.Size(464, 242);
            this.chartSentiment.TabIndex = 1;
            this.chartSentiment.Text = "chart1";
            this.chartSentiment.Click += new System.EventHandler(this.chart1_Click);
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.label3.Size = new System.Drawing.Size(491, 37);
            this.label3.TabIndex = 4;
            this.label3.Text = "WordCloud";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // listVoiceLogs
            // 
            this.listVoiceLogs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listVoiceLogs.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader11});
            this.listVoiceLogs.Font = new System.Drawing.Font("Tahoma", 10F);
            this.listVoiceLogs.FullRowSelect = true;
            this.listVoiceLogs.Location = new System.Drawing.Point(16, 68);
            this.listVoiceLogs.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.listVoiceLogs.MultiSelect = false;
            this.listVoiceLogs.Name = "listVoiceLogs";
            this.listVoiceLogs.Size = new System.Drawing.Size(931, 168);
            this.listVoiceLogs.SmallImageList = this.imageListType;
            this.listVoiceLogs.TabIndex = 0;
            this.listVoiceLogs.UseCompatibleStateImageBehavior = false;
            this.listVoiceLogs.View = System.Windows.Forms.View.Details;
            this.listVoiceLogs.Click += new System.EventHandler(this.listVoiceLogs_Click);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Transaction ID";
            this.columnHeader1.Width = 227;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Encoding";
            this.columnHeader2.Width = 150;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Channels";
            this.columnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader3.Width = 104;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Sample size";
            this.columnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader4.Width = 93;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Sample rate";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader5.Width = 116;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Duration";
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "";
            // 
            // imageListType
            // 
            this.imageListType.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListType.ImageStream")));
            this.imageListType.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListType.Images.SetKeyName(0, "ear_listen.png");
            this.imageListType.Images.SetKeyName(1, "email.png");
            // 
            // tabDetail
            // 
            this.tabDetail.Controls.Add(this.panel1);
            this.tabDetail.Controls.Add(this.tableIndividual);
            this.tabDetail.Location = new System.Drawing.Point(4, 25);
            this.tabDetail.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabDetail.Name = "tabDetail";
            this.tabDetail.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabDetail.Size = new System.Drawing.Size(965, 586);
            this.tabDetail.TabIndex = 1;
            this.tabDetail.Text = "Detail";
            this.tabDetail.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.splitContainer2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 104);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(959, 478);
            this.panel1.TabIndex = 1;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.chartIndividual);
            this.splitContainer2.Panel1.Controls.Add(this.coChartIndividual);
            this.splitContainer2.Panel1.Controls.Add(this.label4);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.cloudIndividual);
            this.splitContainer2.Panel2.Controls.Add(this.listCompliance);
            this.splitContainer2.Panel2.Controls.Add(this.label5);
            this.splitContainer2.Size = new System.Drawing.Size(959, 478);
            this.splitContainer2.SplitterDistance = 482;
            this.splitContainer2.TabIndex = 0;
            // 
            // chartIndividual
            // 
            chartArea2.Name = "ChartArea1";
            this.chartIndividual.ChartAreas.Add(chartArea2);
            this.chartIndividual.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Name = "Legend1";
            this.chartIndividual.Legends.Add(legend1);
            this.chartIndividual.Location = new System.Drawing.Point(0, 61);
            this.chartIndividual.Name = "chartIndividual";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chartIndividual.Series.Add(series1);
            this.chartIndividual.Size = new System.Drawing.Size(482, 417);
            this.chartIndividual.TabIndex = 2;
            this.chartIndividual.Text = "chart1";
            // 
            // coChartIndividual
            // 
            this.coChartIndividual.Dock = System.Windows.Forms.DockStyle.Top;
            this.coChartIndividual.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.coChartIndividual.FormattingEnabled = true;
            this.coChartIndividual.Items.AddRange(new object[] {
            "Pie chart",
            "Bar chart"});
            this.coChartIndividual.Location = new System.Drawing.Point(0, 37);
            this.coChartIndividual.Name = "coChartIndividual";
            this.coChartIndividual.Size = new System.Drawing.Size(482, 24);
            this.coChartIndividual.TabIndex = 1;
            this.coChartIndividual.SelectedIndexChanged += new System.EventHandler(this.coChartIndividual_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.label4.Size = new System.Drawing.Size(482, 37);
            this.label4.TabIndex = 0;
            this.label4.Text = "Call sentiment";
            // 
            // listCompliance
            // 
            this.listCompliance.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader12});
            this.listCompliance.Dock = System.Windows.Forms.DockStyle.Top;
            this.listCompliance.LargeImageList = this.imageListTrueFalse;
            this.listCompliance.Location = new System.Drawing.Point(0, 37);
            this.listCompliance.Name = "listCompliance";
            this.listCompliance.Size = new System.Drawing.Size(473, 137);
            this.listCompliance.SmallImageList = this.imageListTrueFalse;
            this.listCompliance.TabIndex = 2;
            this.listCompliance.UseCompatibleStateImageBehavior = false;
            this.listCompliance.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Compliance point";
            this.columnHeader12.Width = 542;
            // 
            // imageListTrueFalse
            // 
            this.imageListTrueFalse.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListTrueFalse.ImageStream")));
            this.imageListTrueFalse.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListTrueFalse.Images.SetKeyName(0, "cross.png");
            this.imageListTrueFalse.Images.SetKeyName(1, "tick.png");
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Name = "label5";
            this.label5.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.label5.Size = new System.Drawing.Size(473, 37);
            this.label5.TabIndex = 1;
            this.label5.Text = "Compliance";
            // 
            // tableIndividual
            // 
            this.tableIndividual.AutoSize = true;
            this.tableIndividual.ColumnCount = 3;
            this.tableIndividual.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableIndividual.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34F));
            this.tableIndividual.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableIndividual.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableIndividual.Location = new System.Drawing.Point(3, 4);
            this.tableIndividual.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableIndividual.Name = "tableIndividual";
            this.tableIndividual.RowCount = 4;
            this.tableIndividual.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableIndividual.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableIndividual.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableIndividual.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableIndividual.Size = new System.Drawing.Size(959, 100);
            this.tableIndividual.TabIndex = 0;
            // 
            // tabNetworking
            // 
            this.tabNetworking.Controls.Add(this.buttonClearUdpLog);
            this.tabNetworking.Controls.Add(this.buttonTCPClose);
            this.tabNetworking.Controls.Add(this.textTCPPort);
            this.tabNetworking.Controls.Add(this.textTCPServer);
            this.tabNetworking.Controls.Add(this.buttonTCPConnect);
            this.tabNetworking.Controls.Add(this.textSMTP);
            this.tabNetworking.Controls.Add(this.label14);
            this.tabNetworking.Controls.Add(this.buClearLog);
            this.tabNetworking.Controls.Add(this.button1);
            this.tabNetworking.Controls.Add(this.teUDPLog);
            this.tabNetworking.Controls.Add(this.buUDPSend);
            this.tabNetworking.Controls.Add(this.tePort);
            this.tabNetworking.Controls.Add(this.label1);
            this.tabNetworking.Controls.Add(this.buUDPListen);
            this.tabNetworking.Location = new System.Drawing.Point(4, 25);
            this.tabNetworking.Margin = new System.Windows.Forms.Padding(12);
            this.tabNetworking.Name = "tabNetworking";
            this.tabNetworking.Padding = new System.Windows.Forms.Padding(12);
            this.tabNetworking.Size = new System.Drawing.Size(965, 586);
            this.tabNetworking.TabIndex = 2;
            this.tabNetworking.Text = "Networking";
            this.tabNetworking.UseVisualStyleBackColor = true;
            // 
            // buttonTCPClose
            // 
            this.buttonTCPClose.AutoSize = true;
            this.buttonTCPClose.BackColor = System.Drawing.Color.DodgerBlue;
            this.buttonTCPClose.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue;
            this.buttonTCPClose.FlatAppearance.BorderSize = 2;
            this.buttonTCPClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTCPClose.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonTCPClose.ForeColor = System.Drawing.Color.White;
            this.buttonTCPClose.Location = new System.Drawing.Point(122, 108);
            this.buttonTCPClose.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonTCPClose.Name = "buttonTCPClose";
            this.buttonTCPClose.Size = new System.Drawing.Size(117, 38);
            this.buttonTCPClose.TabIndex = 12;
            this.buttonTCPClose.Text = "TCP disconnect";
            this.buttonTCPClose.UseVisualStyleBackColor = false;
            this.buttonTCPClose.Click += new System.EventHandler(this.buttonTCPClose_Click);
            // 
            // textTCPPort
            // 
            this.textTCPPort.Location = new System.Drawing.Point(496, 116);
            this.textTCPPort.Name = "textTCPPort";
            this.textTCPPort.Size = new System.Drawing.Size(100, 24);
            this.textTCPPort.TabIndex = 11;
            this.textTCPPort.Text = "1234";
            // 
            // textTCPServer
            // 
            this.textTCPServer.Location = new System.Drawing.Point(302, 116);
            this.textTCPServer.Name = "textTCPServer";
            this.textTCPServer.Size = new System.Drawing.Size(188, 24);
            this.textTCPServer.TabIndex = 10;
            this.textTCPServer.Text = "megapepper.dynu.com";
            // 
            // buttonTCPConnect
            // 
            this.buttonTCPConnect.AutoSize = true;
            this.buttonTCPConnect.BackColor = System.Drawing.Color.DodgerBlue;
            this.buttonTCPConnect.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue;
            this.buttonTCPConnect.FlatAppearance.BorderSize = 2;
            this.buttonTCPConnect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTCPConnect.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonTCPConnect.ForeColor = System.Drawing.Color.White;
            this.buttonTCPConnect.Location = new System.Drawing.Point(15, 108);
            this.buttonTCPConnect.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonTCPConnect.Name = "buttonTCPConnect";
            this.buttonTCPConnect.Size = new System.Drawing.Size(101, 38);
            this.buttonTCPConnect.TabIndex = 9;
            this.buttonTCPConnect.Text = "TCP connect";
            this.buttonTCPConnect.UseVisualStyleBackColor = false;
            this.buttonTCPConnect.Click += new System.EventHandler(this.buttonTCPConnect_Click);
            // 
            // textSMTP
            // 
            this.textSMTP.Location = new System.Drawing.Point(302, 70);
            this.textSMTP.Name = "textSMTP";
            this.textSMTP.Size = new System.Drawing.Size(328, 24);
            this.textSMTP.TabIndex = 8;
            this.textSMTP.Text = "127.0.0.1";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(255, 73);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(42, 17);
            this.label14.TabIndex = 7;
            this.label14.Text = "SMTP";
            // 
            // buClearLog
            // 
            this.buClearLog.AutoSize = true;
            this.buClearLog.BackColor = System.Drawing.Color.Silver;
            this.buClearLog.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.buClearLog.FlatAppearance.BorderSize = 2;
            this.buClearLog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buClearLog.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buClearLog.ForeColor = System.Drawing.Color.Black;
            this.buClearLog.Location = new System.Drawing.Point(155, 62);
            this.buClearLog.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buClearLog.Name = "buClearLog";
            this.buClearLog.Size = new System.Drawing.Size(86, 38);
            this.buClearLog.TabIndex = 6;
            this.buClearLog.Text = "Clear log";
            this.buClearLog.UseVisualStyleBackColor = false;
            this.buClearLog.Click += new System.EventHandler(this.buClearLog_Click);
            // 
            // button1
            // 
            this.button1.AutoSize = true;
            this.button1.BackColor = System.Drawing.Color.DodgerBlue;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue;
            this.button1.FlatAppearance.BorderSize = 2;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(155, 16);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(93, 38);
            this.button1.TabIndex = 5;
            this.button1.Text = "UDP stop";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // teUDPLog
            // 
            this.teUDPLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.teUDPLog.BackColor = System.Drawing.Color.Black;
            this.teUDPLog.Font = new System.Drawing.Font("Lucida Console", 10F);
            this.teUDPLog.ForeColor = System.Drawing.Color.White;
            this.teUDPLog.Location = new System.Drawing.Point(15, 162);
            this.teUDPLog.Margin = new System.Windows.Forms.Padding(12);
            this.teUDPLog.Multiline = true;
            this.teUDPLog.Name = "teUDPLog";
            this.teUDPLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.teUDPLog.Size = new System.Drawing.Size(933, 413);
            this.teUDPLog.TabIndex = 4;
            this.teUDPLog.WordWrap = false;
            // 
            // buUDPSend
            // 
            this.buUDPSend.AutoSize = true;
            this.buUDPSend.BackColor = System.Drawing.Color.Lime;
            this.buUDPSend.FlatAppearance.BorderColor = System.Drawing.Color.DarkGreen;
            this.buUDPSend.FlatAppearance.BorderSize = 2;
            this.buUDPSend.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buUDPSend.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buUDPSend.ForeColor = System.Drawing.Color.Black;
            this.buUDPSend.Location = new System.Drawing.Point(15, 62);
            this.buUDPSend.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buUDPSend.Name = "buUDPSend";
            this.buUDPSend.Size = new System.Drawing.Size(133, 38);
            this.buUDPSend.TabIndex = 3;
            this.buUDPSend.Text = "UDP broadcast";
            this.buUDPSend.UseVisualStyleBackColor = false;
            this.buUDPSend.Click += new System.EventHandler(this.buUDPSend_Click);
            // 
            // tePort
            // 
            this.tePort.Location = new System.Drawing.Point(302, 21);
            this.tePort.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tePort.Name = "tePort";
            this.tePort.Size = new System.Drawing.Size(116, 24);
            this.tePort.TabIndex = 2;
            this.tePort.Text = "1234";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(255, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "port";
            // 
            // buUDPListen
            // 
            this.buUDPListen.AutoSize = true;
            this.buUDPListen.BackColor = System.Drawing.Color.DodgerBlue;
            this.buUDPListen.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue;
            this.buUDPListen.FlatAppearance.BorderSize = 2;
            this.buUDPListen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buUDPListen.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buUDPListen.ForeColor = System.Drawing.Color.White;
            this.buUDPListen.Location = new System.Drawing.Point(15, 16);
            this.buUDPListen.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buUDPListen.Name = "buUDPListen";
            this.buUDPListen.Size = new System.Drawing.Size(133, 38);
            this.buUDPListen.TabIndex = 0;
            this.buUDPListen.Text = "UDP listen";
            this.buUDPListen.UseVisualStyleBackColor = false;
            this.buUDPListen.Click += new System.EventHandler(this.buUDPListen_Click);
            // 
            // tabVoiceRecorder
            // 
            this.tabVoiceRecorder.Controls.Add(this.panel2);
            this.tabVoiceRecorder.Controls.Add(this.toolStrip2);
            this.tabVoiceRecorder.Location = new System.Drawing.Point(4, 25);
            this.tabVoiceRecorder.Margin = new System.Windows.Forms.Padding(12);
            this.tabVoiceRecorder.Name = "tabVoiceRecorder";
            this.tabVoiceRecorder.Padding = new System.Windows.Forms.Padding(12);
            this.tabVoiceRecorder.Size = new System.Drawing.Size(965, 586);
            this.tabVoiceRecorder.TabIndex = 3;
            this.tabVoiceRecorder.Text = "Samples";
            this.tabVoiceRecorder.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.comboDevices);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(12, 51);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.panel2.Size = new System.Drawing.Size(941, 523);
            this.panel2.TabIndex = 6;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Controls.Add(this.splitter1);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 29);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.panel3.Size = new System.Drawing.Size(941, 494);
            this.panel3.TabIndex = 1;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.button3);
            this.panel5.Controls.Add(this.label13);
            this.panel5.Controls.Add(this.textFtpDir);
            this.panel5.Controls.Add(this.button2);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Controls.Add(this.label11);
            this.panel5.Controls.Add(this.label10);
            this.panel5.Controls.Add(this.textFtpPass);
            this.panel5.Controls.Add(this.textFtpUser);
            this.panel5.Controls.Add(this.textFtpServer);
            this.panel5.Controls.Add(this.listViewEvents);
            this.panel5.Controls.Add(this.label9);
            this.panel5.Controls.Add(this.label8);
            this.panel5.Controls.Add(this.textSampleId);
            this.panel5.Controls.Add(this.label7);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(670, 10);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(271, 484);
            this.panel5.TabIndex = 2;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 427);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(75, 17);
            this.label13.TabIndex = 16;
            this.label13.Text = "Remote dir";
            // 
            // textFtpDir
            // 
            this.textFtpDir.Location = new System.Drawing.Point(107, 424);
            this.textFtpDir.Name = "textFtpDir";
            this.textFtpDir.Size = new System.Drawing.Size(292, 24);
            this.textFtpDir.TabIndex = 15;
            // 
            // button2
            // 
            this.button2.AutoSize = true;
            this.button2.Location = new System.Drawing.Point(107, 454);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(48, 27);
            this.button2.TabIndex = 14;
            this.button2.Text = "Save";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 397);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(93, 17);
            this.label12.TabIndex = 13;
            this.label12.Text = "FTP password";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 367);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(61, 17);
            this.label11.TabIndex = 12;
            this.label11.Text = "FTP user";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 337);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 17);
            this.label10.TabIndex = 11;
            this.label10.Text = "FTP server";
            // 
            // textFtpPass
            // 
            this.textFtpPass.Location = new System.Drawing.Point(107, 394);
            this.textFtpPass.Name = "textFtpPass";
            this.textFtpPass.Size = new System.Drawing.Size(292, 24);
            this.textFtpPass.TabIndex = 10;
            // 
            // textFtpUser
            // 
            this.textFtpUser.Location = new System.Drawing.Point(107, 364);
            this.textFtpUser.Name = "textFtpUser";
            this.textFtpUser.Size = new System.Drawing.Size(292, 24);
            this.textFtpUser.TabIndex = 9;
            // 
            // textFtpServer
            // 
            this.textFtpServer.Location = new System.Drawing.Point(107, 334);
            this.textFtpServer.Name = "textFtpServer";
            this.textFtpServer.Size = new System.Drawing.Size(292, 24);
            this.textFtpServer.TabIndex = 8;
            // 
            // listViewEvents
            // 
            this.listViewEvents.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewEvents.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader14,
            this.columnHeader15});
            this.listViewEvents.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1,
            listViewItem2,
            listViewItem3});
            this.listViewEvents.LargeImageList = this.imageListEvents;
            this.listViewEvents.Location = new System.Drawing.Point(107, 96);
            this.listViewEvents.Name = "listViewEvents";
            this.listViewEvents.Size = new System.Drawing.Size(161, 181);
            this.listViewEvents.SmallImageList = this.imageListEvents;
            this.listViewEvents.TabIndex = 7;
            this.listViewEvents.UseCompatibleStateImageBehavior = false;
            this.listViewEvents.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Event";
            this.columnHeader14.Width = 113;
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "";
            // 
            // imageListEvents
            // 
            this.imageListEvents.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListEvents.ImageStream")));
            this.imageListEvents.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListEvents.Images.SetKeyName(0, "hourglass.png");
            this.imageListEvents.Images.SetKeyName(1, "accept_button.png");
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 96);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 17);
            this.label9.TabIndex = 6;
            this.label9.Text = "Events";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 59);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(95, 17);
            this.label8.TabIndex = 4;
            this.label8.Text = "Transaction Id";
            // 
            // textSampleId
            // 
            this.textSampleId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textSampleId.Location = new System.Drawing.Point(107, 56);
            this.textSampleId.Name = "textSampleId";
            this.textSampleId.Size = new System.Drawing.Size(161, 24);
            this.textSampleId.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.Dock = System.Windows.Forms.DockStyle.Top;
            this.label7.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Name = "label7";
            this.label7.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.label7.Size = new System.Drawing.Size(271, 37);
            this.label7.TabIndex = 2;
            this.label7.Text = "Monitoring";
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(667, 10);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 484);
            this.splitter1.TabIndex = 1;
            this.splitter1.TabStop = false;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.textSampleLog);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(0, 10);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(667, 484);
            this.panel4.TabIndex = 0;
            // 
            // textSampleLog
            // 
            this.textSampleLog.BackColor = System.Drawing.Color.Black;
            this.textSampleLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textSampleLog.ForeColor = System.Drawing.Color.White;
            this.textSampleLog.Location = new System.Drawing.Point(0, 0);
            this.textSampleLog.Multiline = true;
            this.textSampleLog.Name = "textSampleLog";
            this.textSampleLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textSampleLog.Size = new System.Drawing.Size(667, 484);
            this.textSampleLog.TabIndex = 0;
            this.textSampleLog.WordWrap = false;
            // 
            // comboDevices
            // 
            this.comboDevices.Dock = System.Windows.Forms.DockStyle.Top;
            this.comboDevices.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboDevices.FormattingEnabled = true;
            this.comboDevices.Location = new System.Drawing.Point(0, 5);
            this.comboDevices.Name = "comboDevices";
            this.comboDevices.Size = new System.Drawing.Size(941, 24);
            this.comboDevices.TabIndex = 0;
            // 
            // toolStrip2
            // 
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton2,
            this.toolStripSeparator3,
            this.toolStripButton5,
            this.toolStripButton6,
            this.toolStripSeparator4,
            this.toolStripButton7});
            this.toolStrip2.Location = new System.Drawing.Point(12, 12);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(941, 39);
            this.toolStrip2.TabIndex = 5;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton1.Text = "toolStripButton1";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click_1);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton2.Text = "toolStripButton2";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton5.Image")));
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton5.Text = "toolStripButton5";
            this.toolStripButton5.Click += new System.EventHandler(this.toolStripButton5_Click);
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton6.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton6.Image")));
            this.toolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton6.Text = "toolStripButton6";
            this.toolStripButton6.Click += new System.EventHandler(this.toolStripButton6_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton7.Text = "toolStripButton7";
            this.toolStripButton7.ToolTipText = "Auto-load 20x emails for processing";
            this.toolStripButton7.Click += new System.EventHandler(this.toolStripButton7_Click);
            // 
            // tabDatabase
            // 
            this.tabDatabase.Controls.Add(this.teDatabaseLog);
            this.tabDatabase.Controls.Add(this.teConnectionString);
            this.tabDatabase.Controls.Add(this.buConnectDB);
            this.tabDatabase.Location = new System.Drawing.Point(4, 25);
            this.tabDatabase.Margin = new System.Windows.Forms.Padding(12);
            this.tabDatabase.Name = "tabDatabase";
            this.tabDatabase.Padding = new System.Windows.Forms.Padding(12);
            this.tabDatabase.Size = new System.Drawing.Size(965, 586);
            this.tabDatabase.TabIndex = 4;
            this.tabDatabase.Text = "Database";
            this.tabDatabase.UseVisualStyleBackColor = true;
            // 
            // teDatabaseLog
            // 
            this.teDatabaseLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.teDatabaseLog.BackColor = System.Drawing.Color.Black;
            this.teDatabaseLog.Font = new System.Drawing.Font("Lucida Console", 10F);
            this.teDatabaseLog.ForeColor = System.Drawing.Color.Lime;
            this.teDatabaseLog.Location = new System.Drawing.Point(15, 98);
            this.teDatabaseLog.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.teDatabaseLog.Multiline = true;
            this.teDatabaseLog.Name = "teDatabaseLog";
            this.teDatabaseLog.Size = new System.Drawing.Size(933, 464);
            this.teDatabaseLog.TabIndex = 4;
            this.teDatabaseLog.Text = "Server=127.0.0.1; Database=aidb; Uid=aidemo; Pwd=aidemo;\r\nServer=192.168.1.102; P" +
    "ort=3306; Database=aiDB; Uid=aidemo; Pwd=aidemo;\r\nServer=192.168.1.5; Database=a" +
    "iDB; Uid=aidemo; Pwd=aidemo;\r\n\r\n\r\n";
            // 
            // teConnectionString
            // 
            this.teConnectionString.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.teConnectionString.Location = new System.Drawing.Point(15, 62);
            this.teConnectionString.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.teConnectionString.Name = "teConnectionString";
            this.teConnectionString.Size = new System.Drawing.Size(933, 24);
            this.teConnectionString.TabIndex = 3;
            // 
            // buConnectDB
            // 
            this.buConnectDB.AutoSize = true;
            this.buConnectDB.BackColor = System.Drawing.Color.DodgerBlue;
            this.buConnectDB.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue;
            this.buConnectDB.FlatAppearance.BorderSize = 2;
            this.buConnectDB.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buConnectDB.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buConnectDB.ForeColor = System.Drawing.Color.White;
            this.buConnectDB.Location = new System.Drawing.Point(15, 16);
            this.buConnectDB.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buConnectDB.Name = "buConnectDB";
            this.buConnectDB.Size = new System.Drawing.Size(86, 38);
            this.buConnectDB.TabIndex = 2;
            this.buConnectDB.Text = "Connect";
            this.buConnectDB.UseVisualStyleBackColor = false;
            this.buConnectDB.Click += new System.EventHandler(this.buConnectDB_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(6, 649);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Padding = new System.Windows.Forms.Padding(6);
            this.pictureBox1.Size = new System.Drawing.Size(996, 74);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // openAudio
            // 
            this.openAudio.FileName = "openFileDialog1";
            // 
            // buttonClearUdpLog
            // 
            this.buttonClearUdpLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClearUdpLog.AutoSize = true;
            this.buttonClearUdpLog.BackColor = System.Drawing.Color.DodgerBlue;
            this.buttonClearUdpLog.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue;
            this.buttonClearUdpLog.FlatAppearance.BorderSize = 2;
            this.buttonClearUdpLog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonClearUdpLog.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClearUdpLog.ForeColor = System.Drawing.Color.White;
            this.buttonClearUdpLog.Location = new System.Drawing.Point(874, 108);
            this.buttonClearUdpLog.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonClearUdpLog.Name = "buttonClearUdpLog";
            this.buttonClearUdpLog.Size = new System.Drawing.Size(74, 38);
            this.buttonClearUdpLog.TabIndex = 13;
            this.buttonClearUdpLog.Text = "Clear log";
            this.buttonClearUdpLog.UseVisualStyleBackColor = false;
            this.buttonClearUdpLog.Click += new System.EventHandler(this.buttonClearUdpLog_Click);
            // 
            // cloudControl
            // 
            this.cloudControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cloudControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cloudControl.LayoutType = Gma.CodeCloud.Controls.LayoutType.Spiral;
            this.cloudControl.Location = new System.Drawing.Point(3, 37);
            this.cloudControl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cloudControl.MaxFontSize = 36;
            this.cloudControl.MinFontSize = 6;
            this.cloudControl.Name = "cloudControl";
            this.cloudControl.Palette = new System.Drawing.Color[] {
        System.Drawing.Color.DarkRed,
        System.Drawing.Color.DarkBlue,
        System.Drawing.Color.DarkGreen,
        System.Drawing.Color.Navy,
        System.Drawing.Color.DarkCyan,
        System.Drawing.Color.DarkOrange,
        System.Drawing.Color.DarkGoldenrod,
        System.Drawing.Color.DarkKhaki,
        System.Drawing.Color.Blue,
        System.Drawing.Color.Red,
        System.Drawing.Color.Green};
            this.cloudControl.Size = new System.Drawing.Size(445, 278);
            this.cloudControl.TabIndex = 5;
            this.cloudControl.WeightedWords = null;
            // 
            // cloudIndividual
            // 
            this.cloudIndividual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cloudIndividual.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cloudIndividual.LayoutType = Gma.CodeCloud.Controls.LayoutType.Spiral;
            this.cloudIndividual.Location = new System.Drawing.Point(0, 174);
            this.cloudIndividual.MaxFontSize = 42;
            this.cloudIndividual.MinFontSize = 12;
            this.cloudIndividual.Name = "cloudIndividual";
            this.cloudIndividual.Palette = new System.Drawing.Color[] {
        System.Drawing.Color.DarkRed,
        System.Drawing.Color.DarkBlue,
        System.Drawing.Color.DarkGreen,
        System.Drawing.Color.Navy,
        System.Drawing.Color.DarkCyan,
        System.Drawing.Color.DarkOrange,
        System.Drawing.Color.DarkGoldenrod,
        System.Drawing.Color.DarkKhaki,
        System.Drawing.Color.Blue,
        System.Drawing.Color.Red,
        System.Drawing.Color.Green};
            this.cloudIndividual.Size = new System.Drawing.Size(473, 304);
            this.cloudIndividual.TabIndex = 4;
            this.cloudIndividual.WeightedWords = null;
            // 
            // button3
            // 
            this.button3.AutoSize = true;
            this.button3.Location = new System.Drawing.Point(107, 283);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(70, 27);
            this.button3.TabIndex = 17;
            this.button3.Text = "Load log";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // formMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this.tabPages);
            this.Controls.Add(this.pictureBox1);
            this.Font = new System.Drawing.Font("Tahoma", 10F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "formMain";
            this.Padding = new System.Windows.Forms.Padding(6);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AI Contact centre quality assurance demo";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.formMain_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabPages.ResumeLayout(false);
            this.tabSummary.ResumeLayout(false);
            this.tabSummary.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartSentiment)).EndInit();
            this.tabDetail.ResumeLayout(false);
            this.tabDetail.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartIndividual)).EndInit();
            this.tabNetworking.ResumeLayout(false);
            this.tabNetworking.PerformLayout();
            this.tabVoiceRecorder.ResumeLayout(false);
            this.tabVoiceRecorder.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.tabDatabase.ResumeLayout(false);
            this.tabDatabase.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabPages;
        private System.Windows.Forms.TabPage tabSummary;
        private System.Windows.Forms.ListView listVoiceLogs;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.TabPage tabDetail;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartSentiment;
        private System.Windows.Forms.TabPage tabNetworking;
        private System.Windows.Forms.TextBox teUDPLog;
        private System.Windows.Forms.Button buUDPSend;
        private System.Windows.Forms.TextBox tePort;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buUDPListen;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TabPage tabVoiceRecorder;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox coChart;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private Gma.CodeCloud.Controls.CloudControl cloudControl;
        private System.Windows.Forms.Button buClearLog;
        private System.Windows.Forms.TabPage tabDatabase;
        private System.Windows.Forms.TextBox teConnectionString;
        private System.Windows.Forms.Button buConnectDB;
        private System.Windows.Forms.TextBox teDatabaseLog;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.TableLayoutPanel tableIndividual;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartIndividual;
        private System.Windows.Forms.ComboBox coChartIndividual;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toRecord;
        private System.Windows.Forms.ToolStripButton toStop;
        private System.Windows.Forms.ImageList imageListTrueFalse;
        private System.Windows.Forms.ListView listCompliance;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ToolStripSplitButton buttonLoadLogs;
        private System.Windows.Forms.ToolStripMenuItem allToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem voiceLogsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem emailsToolStripMenuItem;
        private System.Windows.Forms.ImageList imageListType;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox comboDevices;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ListView listViewEvents;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textSampleId;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox textSampleLog;
        private System.Windows.Forms.ImageList imageListEvents;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textFtpPass;
        private System.Windows.Forms.TextBox textFtpUser;
        private System.Windows.Forms.TextBox textFtpServer;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textFtpDir;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.OpenFileDialog openAudio;
        private System.Windows.Forms.TextBox textSMTP;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private Gma.CodeCloud.Controls.CloudControl cloudIndividual;
        private System.Windows.Forms.Button buttonTCPClose;
        private System.Windows.Forms.TextBox textTCPPort;
        private System.Windows.Forms.TextBox textTCPServer;
        private System.Windows.Forms.Button buttonTCPConnect;
        private System.Windows.Forms.Button buttonClearUdpLog;
        private System.Windows.Forms.Button button3;
    }
}

