﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.IO.Compression;

namespace AIdemo
{
    public delegate void SocketConnectedHandler(object ASender);
    public delegate void SocketDisconnectedHandler(object ASender);
    public delegate void SocketError(object ASender, string AErrorMessage);
    public delegate void SocketMessageHandler(object ASender, string AMessage);
    public delegate void SocketDataReceivingHandler(object ASender, Int32 AReceived, Int32 ATotal);

    public class TCPSocket
    {
        public event SocketConnectedHandler OnConnected;
        public event SocketDisconnectedHandler OnDisconnected;
        public event SocketError OnError;
        public event SocketMessageHandler OnMessage;

        public Socket _socket;
        private bool _sending = false;

        int _bufferSize = 8192;
        byte[] _data = new byte[8192];
        MemoryStream _socketBuffer;

        public EndPoint RemoteEndPoint { get { return _socket.RemoteEndPoint; } }

        public TCPSocket()
        {
            _socket = null;
            _socketBuffer = new MemoryStream();
        }

        private string _address = "";
        public string Address { get { return _address; } set { _address = value; } }

        private int _port = 0;
        public int Port { get { return _port; } set { _port = value; } }

        public bool IsConnected { get { try { return _socket.Connected; } catch { return false; } } }

        public bool Sending { get { return _sending; } }

        private object _socketObject;
        public object SocketObject { get { return _socketObject; } set { _socketObject = value; } }

        public bool Connect()
        {
            bool result = false;

            if ((_socket == null) || (!_socket.Connected))
            {
                _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            }

            if (IsConnected == true)
            {
                //  already connected, exit
                result = false;
            }
            else
            {
                //  not connected, connect new socket
                try
                {
                    result = true;
                    _socket.Blocking = false;
                    _socket.BeginConnect(_address, _port, new AsyncCallback(Connected), _socket);
                }
                catch (Exception exc)
                {
                    if (OnError != null)
                    {
                        OnError(this, "Socket connect error: " + exc.Message);
                    }
                }
                return result;
            }

            return result;
        }

        public void Close()
        {
            if(OnDisconnected!=null)
                try
                {
                    OnDisconnected(this);
                }
                catch { }
            _socket.Close();
            _socketBuffer.Close();
        }

        public bool SocketBeginAccept()
        {
            if (_socket.Connected)
            {
                _socket.BeginReceive(_data, 0, _bufferSize, SocketFlags.None, new AsyncCallback(ReceiveData), _socket);
                return true;
            }
            else return false;
        }

        void Connected(IAsyncResult iar)
        {
            _socketBuffer = new MemoryStream();
            _socket = (Socket)iar.AsyncState;
            try
            {
                _socket.EndConnect(iar);
                _socket.BeginReceive(_data, 0, _bufferSize, SocketFlags.None, new AsyncCallback(ReceiveData), _socket);
                if (OnConnected != null)
                {
                    OnConnected(this);
                }
            }
            catch (SocketException)
            {
                if (OnError != null) { OnError(this, "Error connecting"); }
            }
            catch (System.ObjectDisposedException)
            {
                if (OnError != null) { OnError(this, "Error connecting"); }
            }
        }

        void ReceiveData(IAsyncResult iar)
        {
            try
            {
                Socket remote = (Socket)iar.AsyncState;
                int received = remote.EndReceive(iar);
                if (received == 0)
                {
                    _socket.Close();
                    if (OnDisconnected != null) OnDisconnected(this);
                }
                else
                {
                    _socketBuffer.Write(_data, 0, received);
                    if (_socketBuffer.Length > 0)
                    {
                        FindMessageCallback f = new FindMessageCallback(FindMessage);
                        f.Invoke();
                    }
                    _socket.BeginReceive(_data, 0, _bufferSize, SocketFlags.None, new AsyncCallback(ReceiveData), _socket);
                }
            }
            catch { }
        }

        private delegate void FindMessageCallback();
        void FindMessage()
        {
            //  message structure
            //  2|textlen|text payload|3
            //  2|0000|text|3

            

            bool completeMessage = false;
            int messageStarts = -1;
            int lengthCompleteMessage = 0;
            int position = 0;
            _socketBuffer.Seek(0, SeekOrigin.Begin);

            byte[] allData = new byte[_socketBuffer.Length];
            _socketBuffer.Read(allData, 0, (int)_socketBuffer.Length);

            for (int i = 0; i < _socketBuffer.Length; i++)
            {
                if (allData[i] == 0x02)
                {
                    messageStarts = i;
                    break;
                }
            }

            if (messageStarts >= 0)
            {
                int minBufferLength = messageStarts + 1 + sizeof(Int32) + 1;
                if (allData.Length >= minBufferLength)
                {
                    // header
                    position = messageStarts + 1;

                    // removed for java oddity
                    //Int32 textLen = BitConverter.ToInt32(_socketBuffer.GetBuffer(),position);

                    byte[] lenText = new byte[4];
                    lenText[0] = allData[position + 3];
                    lenText[1] = allData[position + 2];
                    lenText[2] = allData[position + 1];
                    lenText[3] = allData[position];                    
                    
                    //Int32 textLen = BitConverter.ToInt32(allData, position);
                    Int32 textLen = BitConverter.ToInt32(lenText, 0);
                    position = position + sizeof(Int32);

                    completeMessage = (allData.Length >= minBufferLength + textLen);
                    lengthCompleteMessage = minBufferLength + textLen;

                    if (completeMessage)
                    {
                        string text = string.Empty;
                        if (textLen > 0)
                        {
                            //text = Encoding.UTF8.GetString(_socketBuffer.GetBuffer(), position, textLen);
                            text = Encoding.UTF8.GetString(allData, position, textLen);
                            position = position + textLen;
                            if (OnMessage != null) try { OnMessage(this, text); }
                                catch { }
                        }

                        //  flush current message from buffer and re-check for new data
                        int newBufferLength = (int)_socketBuffer.Length - position;
                        if (newBufferLength > 0)
                        {
                            try
                            {
                                allData = new byte[newBufferLength];
                                _socketBuffer.Position = messageStarts + lengthCompleteMessage;
                                _socketBuffer.Read(allData, 0, newBufferLength);                                
                                _socketBuffer.SetLength(0);
                                _socketBuffer.Write(allData, 0, newBufferLength);
                                FindMessageCallback f = new FindMessageCallback(FindMessage);
                                f.Invoke();
                            }
                            catch
                            {
                                FindMessageCallback f = new FindMessageCallback(FindMessage);
                                f.Invoke();
                            }
                        }
                        else
                        {

                        }

                    }
                }
            }
            //if (_socketBuffer.Length > 0)
            //{
            //    FindMessageCallback f = new FindMessageCallback(FindMessage);
            //    f.Invoke();
            //}
        }
    }
}
